#  2020.6.29 赵崇轩 日报

##  今日完成任务

- 学习了 Vue 的基本语法和标签
- 学习了 SpringBoot 开发的流程，相互之间的调用关系
- 尝试 vue + springboot 在页面显示数据
- 协助小组初步完成接口文档
- 协助小组进行数据库建立

## 存在的主要问题

- 很多东西需要课下学习与练习，遇到很多错误
- 很多标签不熟悉也记不住

##  明日工作计划

- 跟随老师的学习计划
- 跟随项目的进度
- 复习学过的知识

