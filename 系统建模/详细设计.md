# 概要设计

## 1 目录结构

```
-src
	-main
		-java（代码）
			-com
				-qxxl
					-config（配置类）
						...
					-controller（控制类）
						...
					-exception（异常类）
						...
					-interceptor（拦截器）
						...
					-mapper（Mapper）
						...
					-pojo（实体类）
						...
					-service（服务类）
						...
					-utils（工具类）
						...
		-resources（资源）
			-static（静态资源）
				-bootvue（vue相关）
					-css
						...
					-fonts
						...
					-js
						...
				-html（html文件）
					-dept（部门相关）
						...
					-hr（管理相关）
						...
					-job（职位相关）
						...
					-report（报表相关）
						...
					-reportForm（报表相关）
						...
				-img（图片）
					...
			application.yml
	-test（测试）
		-java
			-com
				-qxxl
					-xxxx（测试模块所在的目录名，如:service）
						...
```



## 2 实体类

### 2.1 /src/main/java/com/qxxl/pojo/

​	Employee.java

```java
/**
 * 员工类
 */
// 省略了get、set、toString、equals、hashCode方法
// 使用lombok的注释@Data自动生成上述方法，不必再写
@Data
// 对应数据库表的名称，如"tb_employee"
@Table(name = "tb_employee")
public class Employee {

    /**
     * 下面是员工基本信息
     */
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer employeeId;
    private String employeeName;
    private String idCard;				// 身份证号
    private Boolean sex;				// 员工性别，1表示男0表示女
    private String phoneNum;			// 员工电话
    private Date birthday;				// 员工生日
    private String email;				// 员工邮箱

    /**
     * 下面是员工职位变动信息
     */
    private Date hiredate;				// 员工入职日期
    private Date transferDate;			// 员工调动日期

    /**
     * 下面是员工所在部门岗位信息
     */
    private Integer deptId;				// 员工所在部门id
    private Integer jobId;				// 员工所在职位id
    private Integer originDeptId;		// 员工原部门id
    private Integer originJobId;		// 员工原岗位id

    /**
     * 下面是员工性质和来源
     */
    private EmployeeTypesEnum employeeTypes;		// 员工性质，实习或者正式员工，该变量应该是枚举类型
    private EmployeeSourcesEnum source;				// 员工来源，该变量应该是枚举类型

    /**
     * 下面是薪资信息
     */
    private Integer monthlySalaryId;	// 月薪id。因为工资可能较为复杂，所以是工资类

    /**
     * 下面是额外信息
     */
    private Integer careerId;				// 职业生涯对应的id
    private Integer educationInfoId;		// 教育信息对应的id
    private EmployeeDegreesEnum degree;		// 员工学历，该变量应该是枚举类型
    private EmployeeForeignLanguageAbilityEnum foreignLanguageAbility; // 员工外语能力，该变量应该是枚举类型
    private Boolean married; 				// 禁止使用isMarried作为属性名称，否则可能导致框架出错

}
```

​	Dept.java

```java
/**
 * 部门类
 */
@Data
@Table(name = "tb_dept")
public class Dept{
    
    /**
     * 下面是部门基本信息
     */
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer deptId;				// 部门id
    private String deptName;			// 部门名称
    private Integer employeeNum;			// 部门员工数量。注意，员工发生变动时，此属性要变化！
}
```

​	Job.java

```java
/**
 * 工作职位类
 */
@Data
@Table(name = "tb_job")
public class Job{
    
    /**
     * 下面是职位的基本信息
     */
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer jobId;				// 职位id
    private String jobName;				// 职位名称
    private Integer employeeNum;			// 职位员工数量。注意，员工发生变动时，此属性要变化！
}
```

​	Salary.java

```java
@Data
@Table(name = "tb_salary")
public class Salary{
    
    // 为了今后拓展设置，现阶段除了工资暂时没有额外内容
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer salaryId;
    private Double basicSalary;
}
```

​	Career.java

```java
@Data
@Table(name = "tb_career")
public class Career{
    
    /** 
    * 暂时只设置三个描述职业生涯的字符串，如：
    * career1：2018-2020，x公司担任xx一职
    * career2：2016-2018，y公司担任yy一职
    * career3：2014-2016，z公司担任zz一职
    */
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer careerId;			// id号
    private String career1;
    private String career2;
    private String career3;
}
```

​	EducationInfo.java

```java
@Data
@Table(name = "tb_education_info")
public class EducationInfo{
    
    /** 
     * 教育信息，如：
     * primary：希望小学
     * junior：希望初中
     * senior：希望高中
     * bachelor：希望本科大学，软件工程学士学位
     * master：希望硕士大学，软件工程硕士学位
     * doctor：希望博士大学，计算机科学与技术博士学位
     * other：其他学历
     */
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer educationInfoId;			// id号
    private String primaryDegree;
    private String juniorDegree;
    private String seniorDegree;
    private String bachelorDegree;
    private String masterDegree;
    private String doctorDegree;
    private String otherDegree;
}
```

​	User.java

```java
/**
 * 管理员账户，用于登录系统
 */
@Data
@Table(name = "tb_user")
public class User{
    
    /**
     * 下面是管理员账号基本信息
     */
    // 标志主键
	@Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private String username;			// 管理员账户名
    private String password;			// 管理员账户密码
}
```

​	Talent.java

```java
/**
 * 人才类
 */
@Data
@Table(name = "tb_talent")
public class Talent{
    
    /**
     * 下面是人才基本信息
     */
    @Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer talentId;			// 人才id
	private String talentName;			// 人才姓名
    private String idCard;				// 身份证号
    private Boolean sex;				// 人才性别，1表示男0表示女
    private String phoneNum;			// 人才电话
	private Date birthday;				// 人才生日
    private String email;				// 人才邮箱
}
```

​	FormerEmployee

```java
/**
 * 离职员工类，离职员工不会在员工表中删除，单独设置此表为了查询
 */
@Data
@Table(name = "tb_former_employee")
public class FormerEmployee{
    
    /**
     * 下面是人才基本信息
     */
    @Id    
	// 主键回填
	@KeySql(useGeneratedKeys = true)
    private Integer formerEmployeeId;			// 离职员工id
	private String formerEmployeeName;			// 姓名
    private Boolean sex;						// 性别
    private Date birthday;						// 生日
    private Integer originDeptId;				// 原部门id
    private Integer originJobId;				// 原岗位id
    private Date leavedate;						// 离职日期
}
```



### 2.2 /src/main/java/com/qxxl/mapper/

​	使用了通用Mapper，详细设计在此给出自定义接口函数和自己包含函数的实例，更多的接口函数详情见接口设计一栏。

​	EmployeeMapper.java(interface)

```java
@Component(value = "employeeMapper")
public interface EmployeeMapper extends Mapper<Employee> {
	/** 
	 * 基本sql语句以及由通用Mapper自动生成，无需再写
     * 如果需要自定义，则在此接口下添加方法。
     */
    
    /**
     * 假设增加自定义方法：通过deptId查询员工。
     * 其中，为了节约篇幅，Results省略
     */
    @Select("SELECT * FROM tb_employee WHERE dept_id = ${deptId}")
    @Results(...)
    public List<Employee> selectByDeptId(Integer deptId);
}
```

### 2.3 /src/main/java/com/qxxl/service/

​	EmployeeService.java

```java
@Service
public class EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 该方法是示例方法，更多的接口函数详情见接口设计一栏。
     */
    public Employee queryById(int id){
        return employeeMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 该方法是事务型的示例方法，更多的接口函数详情见接口设计一栏。
     */
    @Transactional
    public void saveEmployee(Employee employee){
        System.out.println("Method saveEmployee Called");
        // 选择性新增，如果属性为空，不会出现在Insert语句上
        employeeMapper.insertSelective(employee);
    }
}
```

## 3 数据库设计

​	数据库设计是依照实体类的。

​	使用通用Mapper，表项名和属性名只要符合驼峰命名和下划线命名都可被对应识别，例userName和user_name可以对应。否则在属性使用：

```java
@Column(name = "xxx")
```

​	来手动进行对应，如：

```java
@Column(name = "name")

private String username; 
```

​	类中的主键属性使用下面的格式：

```java
// 标志主键
@Id    
// 主键回填
@KeySql(useGeneratedKeys = true)
private Integer id;
```

​	同时，基本类型存在默认值，实体类中不推荐使用基本类型（int等）。

### 3.1 员工表

​	表名：tb_employee

​	表项内容为一个员工的信息。内容详细如下：

| 字段名                   | 数据类型         | 约束                          | 说明             |
| :----------------------- | ---------------- | ----------------------------- | ---------------- |
| employee_id              | Integer          | 主键，自增长                  | 员工编号         |
| employee_name            | varchar(64)      |                               | 员工姓名         |
| id_card                  | varchar(18)      |                               | 员工身份证号     |
| sex                      | Integer          | 0女1男                        | 员工性别         |
| phone_num                | varchar(11)      |                               | 员工电话         |
| birthday                 | Date             |                               | 员工生日         |
| email                    | varchar(64)      |                               | 员工邮箱         |
| hiredate                 | Date             |                               | 员工入职日期     |
| transferdate             | Date             |                               | 员工职位变换日期 |
| dept_id                  | Integer          | 外键，普通索引                | 员工所在部门id   |
| job_id                   | Integer          | 外键，普通索引                | 员工所在职位id   |
| origin_dept_id           | Integer          | 外键，普通索引                | 员工原部门id     |
| origin_job_id            | Integer          | 外键，普通索引                | 员工原岗位id     |
| employee_types           | Integer          | 应当用枚举类型实现            | 员工性质         |
| source                   | Integer          | 应当用枚举类型实现            | 员工来源         |
| monthly_salary_id        | Integer          | 外键，普通索引                | 月薪id           |
| career_id                | Integer          | 外键，普通索引                | 职业生涯对应的id |
| education_info_id        | Integer          | 外键，普通索引                | 教育信息对应的id |
| degree                   | Integer          | 应当用枚举类型实现            | 员工学历         |
| foreign_language_ability | Integer          | 应当用枚举类型实现            | 员工外语能力     |
| married                  | tinyint(Boolean) | 禁止使用isMarried作为属性名称 | 是否结婚         |

### 3.2 部门表

​	表名：tb_dept

​	表项内容为一个部门的信息。内容详细如下：

| 字段名       | 数据类型    | 约束         | 说明     |
| ------------ | ----------- | ------------ | -------- |
| dept_id      | Integer     | 主键，自增长 | 部门id   |
| dept_name    | varchar(64) |              | 部门名称 |
| employee_num | Integer     |              | 员工数量 |

### 3.3 职位表

​	表名：tb_job

​	表项内容为一个职位的信息。内容详细如下：

| 字段名       | 数据类型    | 约束         | 说明     |
| ------------ | ----------- | ------------ | -------- |
| job_id       | Integer     | 主键，自增长 | 职位id   |
| job_name     | varchar(64) |              | 职位名称 |
| employee_num | Integer     |              | 员工数量 |

### 3.4 职业生涯表

​	表名：tb_career

​	一个员工的职业生涯信息通过其职工信息当中的careerId来查此表得到。

​	表项内容为一个职业生涯的信息。内容详细如下：

| 字段名    | 数据类型     | 约束         | 说明          |
| --------- | ------------ | ------------ | ------------- |
| career_id | Integer      | 主键，自增长 | id号          |
| career1   | varchar(100) |              | 职业生涯描述1 |
| career2   | varchar(100) |              | 职业生涯描述2 |
| career3   | varchar(100) |              | 职业生涯描述3 |

### 3.5 教育信息表

​	表名：tb_education_info

​	一个员工的教育信息通过其职工信息当中的educationInfoId来查此表得到。

​	表项内容为一个教育信息。内容详细如下：

| 字段名            | 数据类型     | 约束         | 说明         |
| ----------------- | ------------ | ------------ | ------------ |
| education_info_id | Integer      | 主键，自增长 | id号         |
| primary_degree    | varchar(100) |              | 小学名称     |
| junior_degree     | varchar(100) |              | 初中名称     |
| senior_degree     | varchar(100) |              | 高中名称     |
| bachelor_degree   | varchar(100) |              | 本科名称     |
| master_degree     | varchar(100) |              | 硕士学位名称 |
| doctor_degree     | varchar(100) |              | 博士学位名称 |
| other_degree      | varchar(100) |              | 其他学历名称 |

### 3.6 薪资表

​	表名：tb_salary

​	表项内容为一个薪资的信息。内容详细如下：

​	现阶段暂时只有基础工资字段。

| 字段名       | 数据类型 | 约束         | 说明     |
| ------------ | -------- | ------------ | -------- |
| salary_id    | Integer  | 主键，自增长 | id号     |
| basic_salary | Double   |              | 基础工资 |

### 3.7 管理员账号表

​	表名：tb_user

​	表项内容为一个管理员账号的信息。内容详细如下：

| 字段名   | 数据类型    | 约束         | 说明   |
| -------- | ----------- | ------------ | ------ |
| username | varchar(20) | 主键，不为空 | 用户名 |
| password | varchar(20) | 不为空       | 密码   |

### 3.8 人才表

​	表名：tb_talent

​	表项内容为一个人才的信息。内容详细如下：

| 字段名      | 数据类型    | 约束         | 说明       |
| ----------- | ----------- | ------------ | ---------- |
| talent_id   | Integer     | 主键，不为空 | 人才id     |
| talent_name | varchar(64) |              | 人才姓名   |
| id_card     | varchar(18) |              | 人才身份证 |
| sex         | tinyint     | 0女1男       | 人才性别   |
| phone_num   | varchar(11) |              | 人才电话   |
| birthday    | date        |              | 人才生日   |
| email       | varchar(64) |              | 人才邮箱   |

### 3.9 离职员工表

​	表名：tb_former_employee

​	表项内容为一个离职员工的信息。内容详细如下：

| 字段名               | 数据类型    | 约束           | 说明     |
| -------------------- | ----------- | -------------- | -------- |
| former_employee_id   | Integer     | 主键，不为空   | id       |
| former_employee_name | varchar(64) |                | name     |
| sex                  | tinyint     | 0女1男         | sex      |
| birthday             | Date        |                | birthday |
| origin_dept_id       | Integer     | 外键，普通索引 | 原部门id |
| origin_job_id        | Integer     | 外键，普通索引 | 原岗位id |
| leavedate            | Date        |                | 离职日期 |



## 4 使用到的依赖

### 4.1 pom.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <artifactId>spring-boot-starter-parent</artifactId>
        <groupId>org.springframework.boot</groupId>
        <version>2.3.1.RELEASE</version>
    </parent>

    <groupId>com.czx</groupId>
    <artifactId>springbootDemo01</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.alibaba/druid -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.6</version>
        </dependency>

        <!-- 阿里fastjson包JSON转换-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.47</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-thymeleaf</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-configuration-processor -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <!-- 不传递依赖 -->
            <optional>true</optional>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.3.1</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>
        <!--读取excel文件-->
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.17</version>
        </dependency>
        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>3.14</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-jdbc -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.mybatis.spring.boot/mybatis-spring-boot-starter -->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.1.3</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/tk.mybatis/mapper-spring-boot-starter -->
        <dependency>
            <groupId>tk.mybatis</groupId>
            <artifactId>mapper-spring-boot-starter</artifactId>
            <version>2.1.5</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-test -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>2.3.1.RELEASE</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>5.4.17.Final</version>
        </dependency>

        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.3.1</version>
        </dependency>

        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.5</version>
        </dependency>

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.17</version>
        </dependency>

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>3.14</version>
        </dependency>

    </dependencies>

</project>
```

## 5 接口设计

### 5.1 接口文档

关于查找的操作，匹配的信息暂定，可以根据名字，id，生日等。


#### 5.1.1 部门管理

```java
// 返回一个list类型的数据，里面是部门类
List<Dept> deptGetAll();

// 返回一个list类型的数据，里面是部门类是符合匹配信息的部门，传入的参数是想要搜索的信息
List<Dept> deptSearch(String str);

// 新增一个部门，需要传入名字，id自增，人数number为0，要新建一个对应的表（这个表的id自增，并把这个id赋给部门员工表的序好），失败则抛出异常
void deptAdd(String name);

// 删除一个部门，传入该部门的id，需要从部门表中删除记录，并删除这个部门的对应的员工表。失败则抛出异常
void deptDelete(int id);

// 通过id返回一个部门对象，如果id不存在抛出异常
Dept deptGetOne(int id);

// 修改部门的信息，（只能改名字，名字不为空）
void deptUpdate(int id,String name);

// 查看该部门的详细信息，名字，人数，岗位表，员工表
void viewDeptDetail（int id(该部门的id)）;
```

#### 5.1.2 岗位管理

```java
// 返回一个list类型的数据，里面是岗位类
List<Job> jobGetAll();

// 返回一个list类型的数据，里面是岗位类是符合匹配信息的部门，传入的参数是想要搜索的信息
List<Job>jobSearch(String str);

// 新增一个岗位，需要传入名字，id自增，人数number为0，要新建一个对应的表（这个表的id自增，并把这个id赋给岗位员工表的序好），失败则抛出异常
void jobAdd(String name);

//删除一个岗位，传入该岗位的id，需要从岗位表中删除记录，并删除这个岗位的对应的员工表。失败则抛出异常
void deptDelete(int id);

// 通过id返回一个岗位对象，如果id不存在抛出异常
Job jobGetOne(int id);

// 修改部门的信息，（只能改名字，名字不为空）
void jobUpdate(int id,String name);

// 查看该部门的详细信息，名字，人数，岗位表，员工表
void viewJobDetail(int id(该部门的id));
```

#### 5.1.3 人事管理

```java
// 普通入职（在某一个岗位页面点击添加新员工）需要新建一个员工类对象，不成功则抛出异常
void normalEmploy(int id(该岗位的id)，*****（新建员工需要的属性值）);

// 人才库入职（在某一个岗位中点击添加新员工，选择人才库，选择某一个员工，点击确定）,不成功则抛出异常
void specialEmploy(int id(该岗位的id)，int id(人才库的某一位员工的id));
```

#### 5.1.4 试用期管理
```java
// 返回一个试用期员工的列表
List<Employee> getProbationEmp();

// 转正操作，传入一个试用期员工的id，将员工类型修改为正式员工，不成功则抛异常
void empConfirm(int id);

// 延期操作，传入一个试用期员工的id，修改员工的试用期。
void empDelay(int id（员工id），String string（试用期）);

// 期员工列表中得到一个或一组转正的员工
List<Employee> getOneProbationEmp(String string(匹配的信息));
```

#### 5.1.5 部门调动

```java
// 返回该员工,没有找到返回空
Employee empSearch(String string(匹配的信息));

// 部门调动
void deptTransfer（int id(员工id)，int id(要转的部门id)，int id(要转的岗位id));

// 部门调动
Employee deptTranSearch(String string(匹配的信息));
```


#### 5.1.6 岗位调动

```java
// 返回该员工,没有找到返回空
Employee empSearch(String string(匹配的信息));

// 岗位调动操作
void jobTransfer（int id(员工id)，int id(要转的岗位id);

// 查询已调动员工
Employee jobTranSearch(String string(匹配的信息));
```


#### 5.1.7 员工离职

```java
// 在员工的界面点击解雇，将这个员工从对应部门和岗位和总体的员工表中删除，并在离职员工表中加入这个员工
void employeeLeave(int id);

// 没有找到返回空
void FiredSearch(String string(匹配的信息));

// 返回该员工,没有找到返回空
Employee empSearch(String string(匹配的信息));
```

 #### 5.1.8 员工信息管理

```java
// 返回该员工,没有找到返回空
Employee empSearch(String string(匹配的信息));
```

#### 5.1.9 报表管理

```java
// 新聘员工查找，没有返回空
List<Emplpyee> newEmpGetAll(int (0---一个月，1---3个月，2---6个月，3---12个月));

// 离职员工查找，没有返回空
List<Emplpyee> firedEmpGetAll(int (0---一个月，1---3个月，2---6个月，3---12个月));


// 部门调动，没有返回空
List<Emplpyee> deptTranGetAll(int (0---一个月，1---3个月，2---6个月，3---12个月));

// 岗位调动，没有返回空
List<Emplpyee> jobTranGetAll(int (0---一个月，1---3个月，2---6个月，3---12个月));

// 人事月报，遍历上述四个表，找到对应月份的依次打印出来,没有返回空
List<Emplpyee> jobTranGetAll(int month（输入目标月份）)

```

### 5.2 实体类增删改查接口文档

#### 5.2.1 Career

```java
/**
 * 增
 */
public void saveCareer(Career career);
/**
 * 删
 */
public void deleteCareer(Integer id);
/**
 * 改，改动的Career中id不变
 */
public void updateCareer(Career career);
/**
 * 查
 */
public Career queryById(Integer id);
```

#### 5.2.2 Dept

```java
/**
 * 增
 */
public void saveDept(Dept dept);
/**
 * 删
 */
public void deleteDept(Integer id);
/**
 * 改，改动的Dept中id不变
 */
public void updateDept(Dept dept);
/**
 * 查
 */
public Dept queryById(Integer id);
```

#### 5.2.3 EducationInfo

```java
/**
 * 增
 */
public void saveEducationInfo(EducationInfo educationInfo);
/**
 * 删
 */
public void deleteEducationInfo(Integer id);
/**
 * 改，改动的EducationInfo中id不变
 */
public void updateEducationInfo(EducationInfo educationInfo);
/**
 * 查
 */
public EducationInfo queryById(Integer id);
```

#### 5.2.4 Employee

```java
/**
 * 增
 */
public void saveEmployee(Employee employee);
/**
 * 删
 */
public void deleteEmployee(Integer id);
/**
 * 改，改动的Employee中id不变
 */
public void updateEmployee(Employee employee);
/**
 * 查
 */
// Id唯一，只返回一个
public Employee queryById(Integer id);
// 姓名模糊查询，例如输入“小”，返回叫小明、小李、小张等等
public List<Employee> queryByName(String name);
```

#### 5.2.5 FormerEmployee

```java
/**
 * 增
 */
public void saveFormerEmployee(FormerEmployee formerEmployee);
/**
 * 删
 */
public void deleteFormerEmployee(Integer id);
/**
 * 改，改动的Employee中id不变
 */
public void updateFormerEmployee(FormerEmployee formerEmployee);
/**
 * 查
 */
// Id唯一，只返回一个
public FormerEmployee queryById(Integer id);
// 姓名模糊查询，例如输入“小”，返回叫小明、小李、小张等等
public List<FormerEmployee> queryByName(String name);
```

#### 5.2.6 Job

```java
/**
 * 增
 */
public void saveJob(Job job);
/**
 * 删
 */
public void deleteJob(Integer id);
/**
 * 改，改动的Employee中id不变
 */
public void updateJob(Job job);
/**
 * 查
 */
// Id唯一，只返回一个
public Job queryById(Integer id);
// 姓名模糊查询，例如输入“外”，返回外交部、外语部等等
public List<Job> queryByName(String name);
```

#### 5.2.7 Salary

```java
/**
 * 增
 */
public void saveSalary(Salary salary);
/**
 * 删
 */
public void deleteSalary(Integer id);
/**
 * 改，改动的Salary中id不变
 */
public void updateSalary(Salary salary);
/**
 * 查
 */
public Salary queryById(Integer id);
```

#### 5.2.8 Talent

```java
/**
 * 增
 */
public void saveTalent(Talent talent);
/**
 * 删
 */
public void deleteTalent(Integer id);
/**
 * 改，改动的Talent中id不变
 */
public void updateTalent(Talent talent);
/**
 * 查
 */
// Id唯一，只返回一个
public Talent queryById(Integer id);
// 姓名模糊查询，例如输入“小”，返回叫小明、小李、小张等等
public List<Talent> queryByName(String name);
```

#### 5.2.9 User

```java
/**
 * 增
 */
public void saveUser(User user);
/**
 * 删
 */
public void deleteUser(String username);
/**
 * 改，改动的User中username不变
 */
public void updateUser(User user);
/**
 * 查
 */
public User queryByUsername(String username);
```

### 5.3 Service层接口文档

​	一些对数据库操作较为复杂的方法被放在了Service层，下面给出它们的接口，Controller层可以直接调用。

#### 5.3.1 人才库入职

```java
// 该方法位于TalentService内
// talent_id是人才id号；employee用于传递入职需要的信息
/**
 * 方法接口使用说明
 * 参数1：需要入职的人才id
 * 参数2：Employee对象，用于存入一些额外信息，该对象最终将存入tb_employee表中
 * 方法思路：将人才的基本信息整合进employee之中，然后对employee的部分信息修改（入职时间等），最后存入  	employee
 * 使用方式：将除基本信息之外的入职需要的信息，如英语能力等，存入至employee对象中，然后调用该方法
 * 效果：该人才从表tb_talent中移除，整合employee的信息后加入tb_employee表。该员工的来源被设置为人才库，入职时间被设置为今天，该员工的dept和job的属性employeeNum加1，originDeptId和originJobId被设置为null
 */
void talentEmploy(Integer talent_id, Employee employee);
```

#### 5.3.2 普通入职

```java
// 该方法位于EmployeeService内
// employee应当是封装好的一个员工
/**
 * 方法接口使用说明
 * 参数：封装好的员工employee
 * 方法思路：整合修改信息
 * 使用方式：将网页上获取的输入封装进一个Employee对象，然后传入这个对象作为参数
 * 效果：employee的来源设置为普通来源，入职时间设置为今天，该员工的dept和job的属性employeeNum加1，originDeptId和originJobId被设置为null
 */
void normalEmploy(Employee employee);
```

#### 5.3.3 试用期员工转正

```java
// 该方法位于EmployeeService内
/**
 * 参数：employee_id是一个试用期员工的id
 * 效果：如果不是试用期员工，会抛出异常；如果是，修改员工种类为正式员工
 */
void empConfirm(int employee_id);
```

#### 5.3.4 员工岗位调动

```java
// 该方法位于EmployeeService内
/**
 * 参数1：employeeId：需要调动的员工的id
 * 参数2：jobId：新岗位id
 * 效果：员工被调入新岗位，员工的originJobId和jobId被设置，旧岗位人数-1，新岗位人数+1
 */
void jobTransfer(int employee_id, int job_id);
```

#### 5.3.5 员工部门调动

```java
// 该方法位于EmployeeService内
/**
 * 参数1：employeeId：需要调动的员工的id
 * 参数2：deptId：新部门id
 * 效果：员工被调入新部门，员工的originDepId和depId被设置，旧部门人数-1，新部门人数+1
 */
void deptTransfer(int employee_id, int dept_id, int job_id);
```

#### 5.3.6 员工离职

```java
// 该方法位于EmployeeService内
/**
 * 参数：离职员工id
 * 效果：员工离职，从员工表删除，在离职员工表中加入
 */
void empLeave(int employee_id);
```

#### 5.3.7 获取实习员工列表

```java
// 该方法位于EmployeeService内
/**
 * 效果：返回实习员工列表
 */
List<Employee> getProbationaryEmployees();
```

#### 5.3.8 获取指定时间内岗位、部门调动过的员工

```java
// 该方法位于EmployeeService内
/**
 * 参数：时间，月份
 * 效果：返回指定时间内的调动过的员工列表
 */
List<Employee> deptTran(Integer time);
List<Employee> jobTran(Integer time);
```

#### 5.3.9 获取指定时间内新入职的员工

```java
// 该方法位于EmployeeService内
/**
 * 参数：时间，月份
 * 效果：返回指定时间内的新员工列表
 */
List<Employee> monthEmployee(Integer time)
```

### 5.4 错误检测函数接口文档

#### 5.4.1 说明

​	注意，因为错误检测存在一个返回具体错误信息的String类型的返回值，所以错误检测应当在调用具体服务函数之前。错误检测函数检测到错误会返回错误信息，如果没有检测到错误则返回字符串"success"，使用equals来判定返回字符串是否等于"success"，从而确定是否出错。

​	例如，模拟一个保存Employee的过程，这个过程应当检查employee的信息是否合法：

```java
Employee employee = new Emplouyee();

// 一系列设置属性的函数

String info = employeeService.employeeCheck(employee);
if (info.equals("success")) {
    
     // 正确
    employeeService.saveEmployee(employee);
}
else {
    
    // 错误处理，错误信息在info中
    ...; 
}
```

​	再比如，模拟一个部门调用过程，该过程给服务函数传递的参数为employee_id和job_id，所以应当提前检查这两个id是否有具体的数据库项对应：

```java
String info1 = employeeService.employeeIdCheck(employeeIdCheck);
String info2 = jobService.jobIdCheck(targetJobId);
if (info1.equals("success") && info2.equals("success")) {
    
    // 正确
    employeeService.transferJob(employeeId, targetJobId);
}
else {
    
    // 错误处理，错误信息在info中
    ...;
}
```

#### 5.4.2 接口函数

​	下面这一组方法，包含Employee、Talent和FormerEmployee

```java
/**
 * 下面的函数定义在EmployeeService中
 */

// 检查employee整体是否合法，用于保存一个新的employee
public String employeeCheck(Employee employee);

// 检查employeeId是否有一个employee对应，避免调用有些方法出现空指针
public String employeeIdCheck(Integer employeeId);



/**
 * 下面的函数定义在TalentService中
 */

// 检查talent整体是否合法，用于保存一个新的talent
public String talentCheck(Talent talent);

// 检查talentId是否有一个employee对应，避免调用有些方法出现空指针
public String talentIdCheck(Integer talentId);



/**
 * 下面的函数定义在FormerEmployeeService中
 */

// 检查formerEmployee是否有一个formerEmployee对应，避免调用有些方法出现空指针
public String formerEmployeeIdCheck(Integer formerEmployeeId);
```

```java
/**
 * 下面的函数定义在DeptService中
 */

// 检查dept整体是否合法，用于保存一个新的dept
public String deptCheck(Dept dept);

// 检查deptId是否有一个dept对应，避免调用有些方法出现空指针
public String deptIdCheck(Integer id);
```

```java
/**
 * 下面的函数定义在JobService中
 */

// 检查job整体是否合法，用于保存一个新的job
public String jobCheck(Job job);

// 检查jobId是否有一个job对应，避免调用有些方法出现空指针
public String jobIdCheck(Integer id);
```

