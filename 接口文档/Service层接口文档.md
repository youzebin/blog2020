# Service层接口文档

一些对数据库操作较为复杂的方法被放在了Service层，下面给出它们的接口，Controller层可以直接调用。

## 人才库入职

```java
// 该方法位于TalentService内
// talent_id是人才id号；employee用于传递入职需要的信息
/**
 * 方法接口使用说明
 * 参数1：需要入职的人才id
 * 参数2：Employee对象，用于存入一些额外信息，该对象最终将存入tb_employee表中
 * 方法思路：将人才的基本信息整合进employee之中，然后对employee的部分信息修改（入职时间等），最后存入  	employee
 * 使用方式：将除基本信息之外的入职需要的信息，如英语能力等，存入至employee对象中，然后调用该方法
 * 效果：该人才从表tb_talent中移除，整合employee的信息后加入tb_employee表。该员工的来源被设置为人才库，入职时间被设置为今天，该员工的dept和job的属性employeeNum加1，originDeptId和originJobId被设置为null
 */
void talentEmploy(Integer talent_id, Employee employee);
```

## 普通入职

```java
// 该方法位于EmployeeService内
// employee应当是封装好的一个员工
/**
 * 方法接口使用说明
 * 参数：封装好的员工employee
 * 方法思路：整合修改信息
 * 使用方式：将网页上获取的输入封装进一个Employee对象，然后传入这个对象作为参数
 * 效果：employee的来源设置为普通来源，入职时间设置为今天，该员工的dept和job的属性employeeNum加1，originDeptId和originJobId被设置为null
 */
void normalEmploy(Employee employee);
```

## 试用期员工转正

```java
// 该方法位于EmployeeService内
/**
 * 参数：employee_id是一个试用期员工的id
 * 效果：如果不是试用期员工，会抛出异常；如果是，修改员工种类为正式员工
 */
void empConfirm(int employee_id);
```

## 员工岗位调动

```java
// 该方法位于EmployeeService内
/**
 * 参数1：employeeId：需要调动的员工的id
 * 参数2：jobId：新岗位id
 * 效果：员工被调入新岗位，员工的originJobId和jobId被设置，旧岗位人数-1，新岗位人数+1
 */
void jobTransfer(int employee_id, int job_id);
```

## 员工部门调动

```java
// 该方法位于EmployeeService内
/**
 * 参数1：employeeId：需要调动的员工的id
 * 参数2：deptId：新部门id
 * 效果：员工被调入新部门，员工的originDepId和depId被设置，旧部门人数-1，新部门人数+1
 */
void deptTransfer(int employee_id, int dept_id, int job_id);
```

## 员工离职

```java
// 该方法位于EmployeeService内
/**
 * 参数：离职员工id
 * 效果：员工离职，从员工表删除，在离职员工表中加入
 */
void empLeave(int employee_id);
```

