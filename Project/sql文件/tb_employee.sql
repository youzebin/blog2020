/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : qxxl

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 04/07/2020 03:53:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_employee
-- ----------------------------
DROP TABLE IF EXISTS `tb_employee`;
CREATE TABLE `tb_employee`  (
  `employee_id` int NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` int NULL DEFAULT NULL,
  `phone_num` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `hiredate` date NULL DEFAULT NULL,
  `transfer_date` date NULL DEFAULT NULL,
  `dept_id` int NULL DEFAULT NULL,
  `job_id` int NULL DEFAULT NULL,
  `origin_dept_id` int NULL DEFAULT NULL,
  `origin_job_id` int NULL DEFAULT NULL,
  `employee_types` enum('FORMAL','TEMPORARY','TRAINING','PROBATIONARY') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `source` enum('TALENT_POOL','NORMAL','CAMPUS') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `monthly_salary_id` int NULL DEFAULT NULL,
  `career_id` int NULL DEFAULT NULL,
  `education_info_id` int NULL DEFAULT NULL,
  `degree` enum('HIGH','COLLEGE','BACHELOR','MASTER','DOCTOR') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'HIGH',
  `foreign_language_ability` enum('JUNIOR','SENIOR','CET4','CET6','TEM4','TEM8','TOEFL','IELTS') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `married` int(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  PRIMARY KEY (`employee_id`) USING BTREE,
  INDEX `dept_id`(`dept_id`) USING BTREE,
  INDEX `job_id`(`job_id`) USING BTREE,
  INDEX `origin_dept_id`(`origin_dept_id`) USING BTREE,
  INDEX `origin_job_id`(`origin_job_id`) USING BTREE,
  INDEX `monthly_salary_id`(`monthly_salary_id`) USING BTREE,
  INDEX `career_id`(`career_id`) USING BTREE,
  INDEX `education_info_id`(`education_info_id`) USING BTREE,
  CONSTRAINT `tb_employee_ibfk_1` FOREIGN KEY (`dept_id`) REFERENCES `tb_dept` (`dept_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_2` FOREIGN KEY (`job_id`) REFERENCES `tb_job` (`job_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_3` FOREIGN KEY (`origin_dept_id`) REFERENCES `tb_dept` (`dept_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_4` FOREIGN KEY (`origin_job_id`) REFERENCES `tb_job` (`job_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_5` FOREIGN KEY (`monthly_salary_id`) REFERENCES `tb_salary` (`salary_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_6` FOREIGN KEY (`career_id`) REFERENCES `tb_career` (`career_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_employee_ibfk_7` FOREIGN KEY (`education_info_id`) REFERENCES `tb_education_info` (`education_info_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_employee
-- ----------------------------
INSERT INTO `tb_employee` VALUES (1, 'Rick Sanchez', '510802200000000000', 1, '13612345678', '1950-06-09', 'RickSanchez@qq.com', '2015-06-28', '2020-06-27', 1, 1, 2, 2, 'FORMAL', 'TALENT_POOL', 1, 1, 1, 'DOCTOR', 'TOEFL', 0000000001);
INSERT INTO `tb_employee` VALUES (2, 'Morty Smith', '510801200000000000', 1, '18212345678', '2008-01-27', 'MortySmith@163.com', '2020-07-01', NULL, 2, 2, NULL, NULL, 'PROBATIONARY', 'NORMAL', 2, 2, 2, 'HIGH', 'CET6', 0000000000);
INSERT INTO `tb_employee` VALUES (3, 'Squanchy', '112233199946662534', 1, '15312345678', '1965-12-25', 'Squanchy@hust.com', '2020-03-13', NULL, 2, 3, NULL, NULL, 'FORMAL', 'TALENT_POOL', 3, 3, 3, 'HIGH', 'IELTS', 0000000000);
INSERT INTO `tb_employee` VALUES (4, 'BirdPersonGirl', '236549875201365428', 0, '13712345678', '1966-06-06', 'BirdPersonGirl@whu.com', '2016-12-29', NULL, 3, 4, NULL, NULL, 'FORMAL', 'NORMAL', 4, 4, 4, 'MASTER', 'JUNIOR', 0000000001);

SET FOREIGN_KEY_CHECKS = 1;
