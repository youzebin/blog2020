/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : qxxl

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 04/07/2020 03:54:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('czx', 'root');
INSERT INTO `tb_user` VALUES ('lhm', 'root');
INSERT INTO `tb_user` VALUES ('mjy', 'root');
INSERT INTO `tb_user` VALUES ('root', 'root');
INSERT INTO `tb_user` VALUES ('root_test', '123456');
INSERT INTO `tb_user` VALUES ('wzr', 'root');
INSERT INTO `tb_user` VALUES ('yzb', 'root');
INSERT INTO `tb_user` VALUES ('zcx', 'root');

SET FOREIGN_KEY_CHECKS = 1;
