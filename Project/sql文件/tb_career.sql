/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : qxxl

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 04/07/2020 03:53:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_career
-- ----------------------------
DROP TABLE IF EXISTS `tb_career`;
CREATE TABLE `tb_career`  (
  `career_id` int NOT NULL AUTO_INCREMENT,
  `career1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `career2` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `career3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`career_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_career
-- ----------------------------
INSERT INTO `tb_career` VALUES (1, '曾于2010年和C147Morty一起冒险', '无', '无');
INSERT INTO `tb_career` VALUES (2, '曾于2015年供职于不知道叫什么的公司', '无', '无');
INSERT INTO `tb_career` VALUES (3, '曾于1980年效力于银河系联邦', '曾于2000年效力于自由反抗组织', '无');
INSERT INTO `tb_career` VALUES (4, '曾于1970年效力于银河系联邦', '曾于1980年效力于瑞克城', '曾于2000年效力于鸟人帮');

SET FOREIGN_KEY_CHECKS = 1;
