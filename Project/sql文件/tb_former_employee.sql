/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : qxxl

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 04/07/2020 03:53:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_former_employee
-- ----------------------------
DROP TABLE IF EXISTS `tb_former_employee`;
CREATE TABLE `tb_former_employee`  (
  `former_employee_id` int NOT NULL AUTO_INCREMENT,
  `former_employee_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` tinyint NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `origin_dept_id` int NULL DEFAULT NULL,
  `origin_job_id` int NULL DEFAULT NULL,
  `leavedate` date NULL DEFAULT NULL,
  PRIMARY KEY (`former_employee_id`) USING BTREE,
  INDEX `origin_dept_id`(`origin_dept_id`) USING BTREE,
  INDEX `origin_job_id`(`origin_job_id`) USING BTREE,
  CONSTRAINT `tb_former_employee_ibfk_1` FOREIGN KEY (`origin_dept_id`) REFERENCES `tb_dept` (`dept_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_former_employee_ibfk_2` FOREIGN KEY (`origin_job_id`) REFERENCES `tb_job` (`job_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_former_employee
-- ----------------------------
INSERT INTO `tb_former_employee` VALUES (1, 'Jerry Smith', 1, '1980-08-09', 2, 2, '2020-07-02');
INSERT INTO `tb_former_employee` VALUES (2, 'Beth Smith', 0, '1981-05-19', 1, 1, '2020-01-01');
INSERT INTO `tb_former_employee` VALUES (3, 'WWGMK', 1, '2004-10-27', 2, 2, '2020-07-03');

SET FOREIGN_KEY_CHECKS = 1;
