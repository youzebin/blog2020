/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : qxxl

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 04/07/2020 03:53:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_education_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_education_info`;
CREATE TABLE `tb_education_info`  (
  `education_info_id` int NOT NULL AUTO_INCREMENT,
  `primary_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `junior_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `senior_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bachelor_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `master_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `doctor_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `other_degree` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`education_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_education_info
-- ----------------------------
INSERT INTO `tb_education_info` VALUES (1, '四川省成都市西街小学', '四川省广元市东城实验学校', '四川省绵阳中学', '华中科技大学', '无', '无', '翻斗幼儿园乖乖小宝宝');
INSERT INTO `tb_education_info` VALUES (2, '湖北省武汉市华中科技大学附属小学', '湖北省武汉市华中科技大学附属中学', '湖北省武汉市华中科技大学附属中学', '无', '无', '无', '翻斗幼儿园坏蛋小宝宝');
INSERT INTO `tb_education_info` VALUES (3, '湖北省武汉市华中师范大学附属小学', '湖北省武汉市华中师范大学第一附属中学', '湖北省武汉市华中师范大学第一附属中学', '华中师范大学', '武汉大学', '华中科技大学', '翻斗花园究极破坏王');
INSERT INTO `tb_education_info` VALUES (4, '湖北省武汉市武汉大学附属小学', '湖北省武汉市武汉大学附属中学', '湖北省武汉市武汉大学附属高中', '武汉大学', '清华大学', '无', '无');

SET FOREIGN_KEY_CHECKS = 1;
