package com.qxxl.service;

import com.qxxl.pojo.Career;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CareerServiceTest {

    @Autowired
    private CareerService careerService;

    @Test
    public void queryById() {
        System.out.println(careerService.queryById(1));
    }

    @Test
    public void saveCareer() {
        Career career = new Career();
        career.setCareer1("2");
        career.setCareer2("2");
        career.setCareer3("2");
        careerService.saveCareer(career);
    }

    @Test
    public void deleteCareer() {
        careerService.deleteCareer(2);
    }

    @Test
    public void updateCareer() {
        Career career = careerService.queryById(1);
        career.setCareer1("3");
        career.setCareer2("3");
        career.setCareer3("3");
        careerService.updateCareer(career);
    }

    @Test
    public void saveCareerAndReturnId() {

        Career career = new Career();
        System.out.println(careerService.saveCareerAndReturnId(career));
    }
}