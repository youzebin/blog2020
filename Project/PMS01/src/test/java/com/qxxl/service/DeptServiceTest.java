package com.qxxl.service;

import com.qxxl.pojo.Dept;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DeptServiceTest {

    @Autowired
    private DeptService deptService;

    @Test
    public void queryById() {
        System.out.println(deptService.queryById(1));
    }

    @Test
    public void saveDept() {
        Dept dept = new Dept();
        dept.setDeptName("2");
        dept.setEmployeeNum(0);
        deptService.saveDept(dept);
    }

    @Test
    public void deleteDept() {
        deptService.deleteDept(2);
    }

    @Test
    public void updateDept() {
        Dept dept = deptService.queryById(1);
        dept.setDeptName("new");
        dept.setEmployeeNum(2);
        deptService.updateDept(dept);
    }

    @Test
    public void deptGetAll() {
        List<Dept> depts = new ArrayList<Dept>(deptService.deptGetAll());
        for (Dept dept : depts) {
            System.out.println(dept);
        }
    }

    @Test
    public void queryByName() {
        List<Dept> depts = new ArrayList<Dept>(deptService.queryByName("1"));
        for (Dept dept : depts) {
            System.out.println(dept);
        }
    }
}