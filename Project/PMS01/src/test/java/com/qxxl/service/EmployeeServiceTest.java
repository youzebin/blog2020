package com.qxxl.service;

import com.qxxl.pojo.Employee;
import com.qxxl.utils.enums.EmployeeDegreesEnum;
import com.qxxl.utils.enums.EmployeeForeignLanguageAbilityEnum;
import com.qxxl.utils.enums.EmployeeSourcesEnum;
import com.qxxl.utils.enums.EmployeeTypesEnum;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void queryById() {
        System.out.println(employeeService.queryById(1));
    }

    @Test
    public void queryByName(){

        List<Employee> employeesList= new ArrayList<Employee>(employeeService.queryByName("1"));
        for (Employee employee : employeesList) {
            System.out.println(employee);
        }
    }


    @Test
    public void saveEmployee() {
        Employee employee = new Employee();
        employee.setSex(true);
        employee.setDegree(EmployeeDegreesEnum.BACHELOR);
        employee.setSource(EmployeeSourcesEnum.NORMAL);
        employee.setForeignLanguageAbility(EmployeeForeignLanguageAbilityEnum.TEM8);
        employee.setEmployeeTypes(EmployeeTypesEnum.PROBATIONARY);
        employee.setHiredate(new Date());

        employeeService.saveEmployee(employee);
    }

    @Test
    public void deleteEmployee() {

        employeeService.deleteEmployee(3);
    }

    @Test
    public void updateEmployee() {

        Employee employee = employeeService.queryById(2);
        employee.setSex(false);

        employeeService.updateEmployee(employee);
    }

    @Test
    public void normalEmploy(){

        Employee employee = new Employee();
        employee.setDeptId(1);
        employee.setJobId(1);
        employee.setOriginDeptId(1);
        employee.setSex(false);

        employeeService.normalEmploy(employee);
    }

    @Test
    public void empConfirm(){

        employeeService.empConfirm(3);
    }

    @Test
    public void jobTransfer(){

        employeeService.jobTransfer(1, 2);
    }

    @Test
    public void deptTransfer(){

        employeeService.deptTransfer(1, 2);
    }

    @Test
    public void empLeave(){

        employeeService.empLeave(5);
    }

    @Test
    public void queryByDeptId() {

        List<Employee> employeesList= new ArrayList<Employee>(employeeService.queryByDeptId(1));
        for (Employee employee : employeesList) {
            System.out.println(employee);
        }
    }

    @Test
    public void queryByJobId() {

        List<Employee> employeesList= new ArrayList<Employee>(employeeService.queryByJobId(1));
        for (Employee employee : employeesList) {
            System.out.println(employee);
        }
    }

    @Test
    public void getCareer() {

        System.out.println(employeeService.getCareer(1));
    }

    @Test
    public void getProbationaryEmployees() {

        List<Employee> employeesList = new ArrayList<Employee>(employeeService.getProbationaryEmployees());
        for (Employee employee : employeesList) {
            System.out.println(employee);
        }
    }

    @Test
    public void findDeptTran() {

        List<Employee> employees = new ArrayList<Employee>(employeeService.findDeptTran());

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }

    @Test
    public void findJobTran() {

        List<Employee> employees = new ArrayList<Employee>(employeeService.findJobTran());

        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }
}