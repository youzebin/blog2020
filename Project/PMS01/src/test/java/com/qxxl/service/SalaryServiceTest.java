package com.qxxl.service;

import com.qxxl.pojo.Salary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SalaryServiceTest {

    @Autowired
    private SalaryService salaryService;

    @Test
    public void queryById() {
        System.out.println(salaryService.queryById(1));
    }


    @Test
    public void saveSalary() {
        Salary salary = new Salary();
        salary.setBasicSalary(3D);
        salaryService.saveSalary(salary);
    }

    @Test
    public void updateSalary() {
        Salary salary = salaryService.queryById(1);
        salary.setBasicSalary(1D);
        salaryService.updateSalary(salary);
    }

    @Test
    public void deleteSalary() {
        salaryService.deleteSalary(2);
    }

    @Test
    public void saveSalaryAndReturnId() {

        Salary salary = new Salary();
        salary.setBasicSalary(1000D);
        System.out.println(salaryService.saveSalaryAndReturnId(salary));
    }
}