package com.qxxl.service;

import com.qxxl.pojo.FormerEmployee;
import com.qxxl.pojo.Job;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JobServiceTest {

    @Autowired
    private JobService jobService;

    @Test
    public void queryById() {
        System.out.println(jobService.queryById(1));
    }


    @Test
    public void queryByName() {
        List<Job> jobList= new ArrayList<Job>(jobService.queryByName("1"));
        for (Job job : jobList) {
            System.out.println(job);
        }
    }

    @Test
    public void saveJob() {
        Job job = new Job();
        job.setJobName("new");
        jobService.saveJob(job);
    }

    @Test
    public void deleteJob() {
        jobService.deleteJob(3);
    }

    @Test
    public void updateJob() {
        Job job = jobService.queryById(2);
        job.setEmployeeNum(1);
        jobService.updateJob(job);
    }

    @Test
    public void jobGetAll() {
        List<Job> jobList= new ArrayList<Job>(jobService.jobGetAll());
        for (Job job : jobList) {
            System.out.println(job);
        }
    }
}