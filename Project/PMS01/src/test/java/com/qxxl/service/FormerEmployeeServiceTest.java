package com.qxxl.service;

import com.qxxl.pojo.Employee;
import com.qxxl.pojo.FormerEmployee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FormerEmployeeServiceTest {

    @Autowired
    private FormerEmployeeService formerEmployeeService;

    @Test
    public void queryById() {
        System.out.println(formerEmployeeService.queryById(1));
    }

    @Test
    public void queryByName() {
        List<FormerEmployee> formerEmployeeList= new ArrayList<FormerEmployee>(formerEmployeeService.queryByName("1"));
        for (FormerEmployee formerEmployee : formerEmployeeList) {
            System.out.println(formerEmployee);
        }
    }

    @Test
    public void saveFormerEmployee() {
        FormerEmployee formerEmployee = new FormerEmployee();
        formerEmployee.setOriginJobId(1);
        formerEmployee.setOriginDeptId(1);
        formerEmployee.setSex(true);

        formerEmployeeService.saveFormerEmployee(formerEmployee);
    }

    @Test
    public void deleteFormerEmployee() {

        formerEmployeeService.deleteFormerEmployee(3);
    }

    @Test
    public void updateFormerEmployee() {

        FormerEmployee formerEmployee = formerEmployeeService.queryById(2);
        formerEmployee.setSex(true);
        formerEmployeeService.updateFormerEmployee(formerEmployee);
    }
}