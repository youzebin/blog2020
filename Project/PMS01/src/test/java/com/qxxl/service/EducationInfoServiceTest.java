package com.qxxl.service;

import com.qxxl.pojo.EducationInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EducationInfoServiceTest {

    @Autowired
    private EducationInfoService educationInfoService;

    @Test
    public void queryById() {
        System.out.println(educationInfoService.queryById(1));
    }

    @Test
    public void saveEducationInfo() {
        EducationInfo educationInfo = new EducationInfo();
        educationInfo.setBachelorDegree("2");
        educationInfo.setDoctorDegree("2");
        educationInfo.setJuniorDegree("2");

        educationInfoService.saveEducationInfo(educationInfo);
    }

    @Test
    public void deleteEducationInfo() {

        educationInfoService.deleteEducationInfo(2);
    }

    @Test
    public void updateEducationInfo() {
        EducationInfo educationInfo = educationInfoService.queryById(1);
        educationInfo.setJuniorDegree("new");

        educationInfoService.updateEducationInfo(educationInfo);
    }

    @Test
    public void saveEducationInfoAndReturnId() {

        EducationInfo educationInfo = new EducationInfo();
        System.out.println(educationInfoService.saveEducationInfoAndReturnId(educationInfo));
    }
}