package com.qxxl.service;

import com.qxxl.pojo.Employee;
import com.qxxl.pojo.Job;
import com.qxxl.pojo.Talent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TalentServiceTest {

    @Autowired
    private TalentService talentService;

    @Test
    public void queryById() {
        System.out.println(talentService.queryById(1));
    }

    @Test
    public void queryByName() {
        List<Talent> talentList= new ArrayList<Talent>(talentService.queryByName("陈"));
        for (Talent talent : talentList) {
            System.out.println(talent);
        }
    }

    @Test
    public void saveTalent() {
        Talent talent = new Talent();
        talent.setTalentName("new");

        talentService.saveTalent(talent);
    }

    @Test
    public void deleteTalent() {
        talentService.deleteTalent(3);
    }

    @Test
    public void updateTalent() {
        Talent talent = talentService.queryById(2);
        talent.setSex(true);
        talentService.updateTalent(talent);
    }

    @Test
    public void talentEmploy(){

        Employee employee = new Employee();
        employee.setCareerId(2);
        employee.setJobId(1);
        employee.setDeptId(1);
        //talentService.talentEmploy(4, employee);

    }
}