package com.qxxl.pojo;

import com.qxxl.utils.enums.EmployeeDegreesEnum;
import com.qxxl.utils.enums.EmployeeForeignLanguageAbilityEnum;
import com.qxxl.utils.enums.EmployeeSourcesEnum;
import com.qxxl.utils.enums.EmployeeTypesEnum;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Table(name = "tb_employee")
public class Employee {

    /**
     * 下面是员工基本信息
     */
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer employeeId;
    private String employeeName;
    private String idCard;				// 身份证号
    private Boolean sex;				// 员工性别，1表示男0表示女
    private String phoneNum;			// 员工电话
    private Date birthday;				// 员工生日
    private String email;				// 员工邮箱

    /**
     * 下面是员工职位变动信息
     */
    private Date hiredate;

    // 员工入职日期
    private Date transferDate;			// 员工调动日期

    /**
     * 下面是员工所在部门岗位信息
     */
    private Integer deptId;				// 员工所在部门id
    private Integer jobId;				// 员工所在职位id
    private Integer originDeptId;		// 员工原部门id
    private Integer originJobId;		// 员工原岗位id

    /**
     * 下面是员工性质和来源
     */
    private EmployeeTypesEnum employeeTypes;		// 员工性质，实习或者正式员工，该变量应该是枚举类型
    private EmployeeSourcesEnum source;				// 员工来源，该变量应该是枚举类型

    /**
     * 下面是薪资信息
     */
    private Integer monthlySalaryId;	// 月薪id。因为工资可能较为复杂，所以是工资类

    /**
     * 下面是额外信息
     */
    private Integer careerId;				// 职业生涯对应的id
    private Integer educationInfoId;		// 教育信息对应的id
    private EmployeeDegreesEnum degree;		// 员工学历，该变量应该是枚举类型
    private EmployeeForeignLanguageAbilityEnum foreignLanguageAbility; // 员工外语能力，该变量应该是枚举类型
    private Boolean married; 				// 禁止使用isMarried作为属性名称，否则可能导致框架出错

}
