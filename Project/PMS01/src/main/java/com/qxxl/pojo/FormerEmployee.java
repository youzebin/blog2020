package com.qxxl.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 离职员工类，离职员工不会在员工表中删除，单独设置此表为了查询
 */
@Data
@Table(name = "tb_former_employee")
public class FormerEmployee{

    /**
     * 下面是人才基本信息
     */
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    private Integer formerEmployeeId;			// 离职员工id
    private String formerEmployeeName;			// 姓名
    private Boolean sex;						// 性别
    private Date birthday;						// 生日
    private Integer originDeptId;				// 原部门id
    private Integer originJobId;				// 原岗位id
    private Date leavedate;						// 离职日期
}