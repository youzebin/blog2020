package com.qxxl.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@Table(name = "tb_user")
public class User {

    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private String username;
    private String password;
}
