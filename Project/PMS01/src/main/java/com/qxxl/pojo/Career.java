package com.qxxl.pojo;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "tb_career")
public class Career {

    /**
     * 暂时只设置三个描述职业生涯的字符串，如：
     * career1：2018-2020，x公司担任xx一职
     * career2：2016-2018，y公司担任yy一职
     * career3：2014-2016，z公司担任zz一职
     */
    // 标志主键
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer careerId;			// id号
    private String career1;
    private String career2;
    private String career3;
}
