package com.qxxl.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "tb_salary")
public class Salary {

    // 为了今后拓展设置，现阶段除了工资暂时没有额外内容
    // 标志主键
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer salaryId;
    private Double basicSalary;
}
