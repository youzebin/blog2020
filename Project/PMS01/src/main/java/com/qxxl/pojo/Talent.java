package com.qxxl.pojo;

import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 人才类
 */
@Data
@Table(name = "tb_talent")
public class Talent {

    /**
     * 下面是人才基本信息
     */
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    private Integer talentId;			// 人才id
    private String talentName;			// 人才姓名
    private String idCard;				// 身份证号
    private Boolean sex;				// 人才性别，1表示男0表示女
    private String phoneNum;			// 人才电话
    private Date birthday;				// 人才生日
    private String email;				// 人才邮箱
}
