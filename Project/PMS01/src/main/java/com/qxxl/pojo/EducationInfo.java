package com.qxxl.pojo;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "tb_education_info")
public class EducationInfo {

    /**
     * 教育信息，如：
     * primary：希望小学
     * junior：希望初中
     * senior：希望高中
     * bachelor：希望本科大学，软件工程学士学位
     * master：希望硕士大学，软件工程硕士学位
     * doctor：希望博士大学，计算机科学与技术博士学位
     * other：其他学历
     */
    // 标志主键
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer educationInfoId;			// id号
    private String primaryDegree;
    private String juniorDegree;
    private String seniorDegree;
    private String bachelorDegree;
    private String masterDegree;
    private String doctorDegree;
    private String otherDegree;
}
