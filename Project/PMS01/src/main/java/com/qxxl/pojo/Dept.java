package com.qxxl.pojo;


import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "tb_dept")
public class Dept {

    /**
     * 下面是部门基本信息
     */
    // 标志主键
    @Id
    // 主键回填
    @KeySql(useGeneratedKeys = true)
    // 只要符合驼峰规则，则不需要@Column。
    // 当字段名称不一致时，@Column(name = "abc)
    private Integer deptId;				// 部门id
    private String deptName;			// 部门名称
    private Integer employeeNum;			// 部门员工数量。注意，员工发生变动时，此属性要变化！
}
