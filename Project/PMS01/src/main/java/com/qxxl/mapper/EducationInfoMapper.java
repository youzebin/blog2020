package com.qxxl.mapper;

import com.qxxl.pojo.EducationInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component(value = "educationInfoMapper")
public interface EducationInfoMapper extends Mapper<EducationInfo> {

    @Select("SELECT LAST_INSERT_ID()")
    public Integer returnLastId();
}
