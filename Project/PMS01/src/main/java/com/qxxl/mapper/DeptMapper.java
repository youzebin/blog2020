package com.qxxl.mapper;

import com.qxxl.pojo.Dept;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "deptMapper")
public interface DeptMapper extends Mapper<Dept> {

    @Select("SELECT * FROM tb_dept WHERE dept_name LIKE '%${name}%'")
    @Results(id="userResult",value={
            @Result(id=true,column = "dept_id",property="deptId"),
            @Result(column = "dept_name",property = "deptName"),
            @Result(column = "employee_num",property = "employeeNum")})
    public List<Dept> selectByName(String name);
}
