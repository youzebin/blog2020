package com.qxxl.mapper;

import com.qxxl.pojo.Talent;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "talentMapper")
public interface TalentMapper extends Mapper<Talent> {

    @Select("SELECT * FROM tb_talent WHERE talent_name LIKE '%${name}%'")
    @Results(id="userResult",value={
            @Result(id=true,column = "talent_id",property="talentId"),
            @Result(column = "talent_name",property = "talentName"),
            @Result(column = "id_card",property = "idCard"),
            @Result(column = "sex",property = "sex"),
            @Result(column = "phone_num",property = "phoneNum"),
            @Result(column = "birthday",property = "birthday"),
            @Result(column = "email",property = "email")
    })
    public List<Talent> selectByName(String name);

    @Select("SELECT * FROM tb_talent")
    @ResultMap("userResult")
    public List<Talent> findAll();
}
