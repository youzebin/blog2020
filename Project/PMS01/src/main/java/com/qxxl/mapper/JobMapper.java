package com.qxxl.mapper;

import com.qxxl.pojo.Job;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "jobMapper")
public interface JobMapper extends Mapper<Job> {

    @Select("SELECT * FROM tb_job WHERE job_name LIKE '%${name}%'")
    @Results(id="userResult",value={
            @Result(id=true,column = "job_id",property="jobId"),
            @Result(column = "job_name",property = "jobName"),
            @Result(column = "employee_num",property = "employeeNum")})
    public List<Job> selectByName(String name);
}
