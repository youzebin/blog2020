package com.qxxl.mapper;

import com.qxxl.pojo.FormerEmployee;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "formerEmployeeMapper")
public interface FormerEmployeeMapper extends Mapper<FormerEmployee> {
    @Select("SELECT * FROM tb_former_employee WHERE former_employee_name LIKE '%${name}%'")
    @Results(id = "userResult", value = {
            @Result(id = true, column = "former_employee_id", property = "formerEmployeeId"),
            @Result(column = "former_employee_name", property = "formerEmployeeName"),
            @Result(column = "sex", property = "sex"),
            @Result(column = "birthday", property = "birthday"),
            @Result(column = "origin_dept_id", property = "originDeptId"),
            @Result(column = "origin_job_id", property = "originJobId"),
            @Result(column = "leavedate", property = "leavedate")
    })
    public List<FormerEmployee> selectByName(String name);
    @Select("SELECT * FROM tb_former_employee WHERE origin_dept_id = '${deptId}'")
    @ResultMap("userResult")
    public List<FormerEmployee> selectByDept(Integer deptId);

    @Select("SELECT * FROM tb_former_employee")
    @ResultMap("userResult")
    public List<FormerEmployee> findAll();

}
