package com.qxxl.mapper;

import com.qxxl.pojo.Dept;
import com.qxxl.pojo.Salary;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "salaryMapper")
public interface SalaryMapper extends Mapper<Salary> {

    @Select("SELECT LAST_INSERT_ID()")
    public Integer returnLastId();
}
