package com.qxxl.mapper;

import com.qxxl.pojo.User;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component(value = "userMapper")
public interface UserMapper extends Mapper<User> {

}
