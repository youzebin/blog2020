package com.qxxl.mapper;

import com.qxxl.pojo.Career;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;
import org.springframework.stereotype.Repository;



@Component(value = "careerMapper")
public interface CareerMapper extends Mapper<Career> {

    @Select("SELECT LAST_INSERT_ID()")
    public Integer returnLastId();
}
