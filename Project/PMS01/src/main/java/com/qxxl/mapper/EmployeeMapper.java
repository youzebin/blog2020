package com.qxxl.mapper;

import com.qxxl.pojo.Employee;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@Component(value = "employeeMapper")
public interface EmployeeMapper extends Mapper<Employee> {

    @Select("SELECT * FROM tb_employee WHERE employee_name LIKE '%${name}%'")
    @Results(id = "userResult", value = {
            @Result(id = true, column = "employee_id", property = "employeeId"),
            @Result(column = "employee_name", property = "employeeName"),
            @Result(column = "id_card", property = "idCard"),
            @Result(column = "sex", property = "sex"),
            @Result(column = "phone_num", property = "phoneNum"),
            @Result(column = "birthday", property = "birthday"),
            @Result(column = "email", property = "email"),
            @Result(column = "hiredate", property = "hiredate"),
            @Result(column = "transferdate", property = "transferDate"),
            @Result(column = "dept_id", property = "deptId"),
            @Result(column = "job_id", property = "jobId"),
            @Result(column = "origin_dept_id", property = "originDeptId"),
            @Result(column = "origin_job_id", property = "originJobId"),
            @Result(column = "employee_types", property = "employeeTypes"),
            @Result(column = "source", property = "source"),
            @Result(column = "monthly_salary_id", property = "monthlySalaryId"),
            @Result(column = "career_id", property = "careerId"),
            @Result(column = "education_info_id", property = "educationInfoId"),
            @Result(column = "degree", property = "degree"),
            @Result(column = "foreign_language_ability", property = "foreignLanguageAbility"),
            @Result(column = "married", property = "married")
    })
    public List<Employee> selectByName(String name);

    @Select("SELECT * FROM tb_employee WHERE dept_id = ${deptId}")
    @ResultMap("userResult")
    public List<Employee> selectByDeptId(Integer deptId);

    @Select("SELECT * FROM tb_employee WHERE job_id = ${jobId}")
    @ResultMap("userResult")
    public List<Employee> selectByJobId(Integer jobId);


    @Select("SELECT * FROM tb_employee")
    @ResultMap("userResult")
    public List<Employee> findAll();

    @Select("SELECT * FROM tb_employee where origin_dept_id is not null")
    @ResultMap("userResult")
    public List<Employee> findDeptTran();

    @Select("SELECT * FROM tb_employee WHERE origin_job_id is not null")
    @ResultMap("userResult")
    public List<Employee> findJobTran();
}
