package com.qxxl.controller;


import com.qxxl.pojo.Dept;
import com.qxxl.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.List;

@RestController
@RequestMapping("/dept")
public class DeptController {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private DeptService deptService;

    @CrossOrigin
    @RequestMapping("/saveDept")
    public void saveDept(@RequestBody Dept dept) {

        deptService.saveDept(dept);
    }

    @CrossOrigin
    @RequestMapping("/queryById")
    public Dept queryById(Integer id) {

        return deptService.queryById(id);
    }

    @CrossOrigin
    @RequestMapping("/queryByName")
    public List<Dept> queryByName(String deptName) {

        return deptService.queryByName(deptName);
    }

    @CrossOrigin
    @RequestMapping("/deleteDept")
    public void deleteDept(Integer id) {

        deptService.deleteDept(id);
    }

    @CrossOrigin
    @RequestMapping("/updateDept")
    public void updateDept(Dept dept) {

        deptService.updateDept(dept);
    }


    @CrossOrigin
    @GetMapping("/findAll")
    public List<Dept> findAll() {

        return deptService.deptGetAll();
    }
}
