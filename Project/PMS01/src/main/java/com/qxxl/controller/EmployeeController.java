package com.qxxl.controller;

import com.qxxl.pojo.Employee;
import com.qxxl.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.List;

@RestController
@RequestMapping("/emp")
public class EmployeeController {

    @Autowired
    private DataSource dataSource;

    @Autowired
    EmployeeService employeeService;

    /**
     * 普通入职
     * @param employee
     */
    @CrossOrigin
    @RequestMapping("/normalEmploy")
    public void normalEmploy(@RequestBody Employee employee) {

        employeeService.normalEmploy(employee);
    }

    /**
     * 保存员工
     * @param employee
     */
    @CrossOrigin
    @RequestMapping("/saveEmployee")
    public void saveEmployee(Employee employee) {

        System.out.println(employee.getJobId() + "=============================================");
        System.out.println(employee.getEmployeeName() + "=============================================");
        employeeService.saveEmployee(employee);
    }

    /**
     * 试用期转正
     * @param employeeId
     */
    @CrossOrigin
    @RequestMapping("/employeeConfirm")
    public void employeeConfirm(int employeeId) {

        employeeService.empConfirm(employeeId);
    }

    /**
     * 岗位调动
     * @param employeeId
     * @param jobId
     */
    @CrossOrigin
    @RequestMapping("/jobTransfer")
    public void jobTransfer(int employeeId, int jobId) {

        employeeService.jobTransfer(employeeId, jobId);
    }

    /**
     * 部门调动
     * @param employeeId
     * @param deptId
     */
    @CrossOrigin
    @RequestMapping("/deptTransfer")
    public void deptTransfer(int employeeId, int deptId) {

        employeeService.deptTransfer(employeeId, deptId);
    }

    /**
     * 离职
     * @param employeeId
     */
    @CrossOrigin
    @RequestMapping("/empLeave")
    public void empLeave(int employeeId) {

        employeeService.empLeave(employeeId);
    }

    /**
     * 通过id搜索
     * @param employeeId
     * @return
     */
    @CrossOrigin
    @RequestMapping("/empSearchById")
    public Employee empSearchById(Integer employeeId) {
        return employeeService.queryById(employeeId);
    }

    /**
     * 姓名模糊搜索
     * @param employeeName
     * @return
     */
    @CrossOrigin
    @RequestMapping("/empSearchByName")
    public List<Employee> empSearchByName(String employeeName) {
        return employeeService.queryByName(employeeName);
    }

    /**
     * 查找所有员工
     * @return
     */
    @CrossOrigin
    @RequestMapping("/findAll")
    public  List<Employee> findAll(){
        return employeeService.findAll();
    }


    /**
     * 通过部门找员工
     * @param id
     * @return
     */
    @CrossOrigin
    @RequestMapping("/findByDept")
    public  List<Employee> findByDept(Integer id){
        return employeeService.queryByDeptId(id);
    }

    /**
     * 通过岗位找员工
     * @param id
     * @return
     */
    @CrossOrigin
    @RequestMapping("/findByJob")
    public  List<Employee> queryByJobId(Integer id){
        return employeeService.queryByJobId(id);
    }

    /**
     * 返回试用期员工列表
     * @return
     */
    @CrossOrigin
    @RequestMapping("/probationaryEmployees")
    public List<Employee> getProbationaryEmployees(){

        return employeeService.getProbationaryEmployees();
    }

    /**
     * 返回正式员工列表
     * @return
     */
    @CrossOrigin
    @RequestMapping("/formalEmployees")
    public List<Employee> getFormalEmployees(){

        return employeeService.getFormalEmployees();
    }


    /**
     * 返回部门更换员工列表
     * @return
     */
    @CrossOrigin
    @RequestMapping("/deptChange")
    public List<Employee> findDeptTran(){

        return employeeService.findDeptTran();
    }

    /**
     * 返回岗位更换员工列表
     * @return
     */
    @CrossOrigin
    @RequestMapping("/jobChange")
    public List<Employee> findJobTran(){

        return employeeService.findJobTran();
    }

    /**
     * 更新一个employee
     *
     * @param employee
     */
    @CrossOrigin
    @RequestMapping("/updateEmployee")
    public void updateEmployee(@RequestBody Employee employee) {
        employeeService.updateEmployee(employee);
    }
}

