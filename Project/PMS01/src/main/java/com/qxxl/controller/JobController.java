package com.qxxl.controller;


import com.qxxl.pojo.Dept;
import com.qxxl.pojo.Job;
import com.qxxl.service.DeptService;
import com.qxxl.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.List;

@RestController
@RequestMapping("/job")
public class JobController {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private JobService jobService;

    /**
     * 保存一个岗位
     * @param job
     */
    @CrossOrigin
    @RequestMapping("/saveJob")
    public void saveJob(@RequestBody Job job) {

        jobService.saveJob(job);
    }

    /**
     * 通过id查找
     * @param id
     * @return
     */
    @CrossOrigin
    @RequestMapping("/queryById")
    public Job queryById(Integer id) {

        return jobService.queryById(id);
    }

    /**
     * 通过名称模糊查找
     * @param jobName
     * @return
     */
    @CrossOrigin
    @RequestMapping("/queryByName")
    public List<Job> queryByName(String jobName) {

        return jobService.queryByName(jobName);
    }

    /**
     * 删除岗位
     * @param id
     */
    @CrossOrigin
    @RequestMapping("/deleteJob")
    public void deleteJob(Integer id) {

        jobService.deleteJob(id);
    }

    /**
     * 更新岗位
     * @param job
     */
    @CrossOrigin
    @RequestMapping("/updateJob")
    public void updateJob(Job job) {

        jobService.updateJob(job);
    }

    /**
     * 查找所有岗位
     * @return
     */
    @CrossOrigin
    @GetMapping("/findAll")
    public List<Job> findAll() {

        System.out.println(jobService.jobGetAll());
        return jobService.jobGetAll();
    }
}
