package com.qxxl.controller;
import com.qxxl.pojo.Employee;
import com.qxxl.pojo.Talent;
import com.qxxl.service.EmployeeService;
import com.qxxl.service.TalentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.util.List;

@RestController
@RequestMapping("/emp")
public class TalentController {
    @Autowired
    private DataSource dataSource;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TalentService talentService;
    /**
     * 人才库入职
     * @param id
     */
    @CrossOrigin
    @RequestMapping("/talentEmploy")
    public void normalEmploy(Integer id) {
        talentService.talentEmploy(id);
    }

    /**
     * 查找所有员工
     * @return
     */
    @CrossOrigin
    @RequestMapping("/talfindAll")
    public  List<Talent> findAll(){
        return talentService.findAll();
    }

}
