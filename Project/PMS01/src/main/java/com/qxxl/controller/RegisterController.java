package com.qxxl.controller;


import com.qxxl.pojo.User;
import com.qxxl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class RegisterController {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @GetMapping("/save")
    @ResponseBody
    public String register(User user){
        User tem_user = userService.queryByUsername(user.getUsername());
        if(tem_user == null){
            userService.saveUser(user);
            return "success";
        }
        else{
            return "error";
        }
    }
}
