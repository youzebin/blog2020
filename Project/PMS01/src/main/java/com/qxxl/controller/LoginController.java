package com.qxxl.controller;


import com.qxxl.pojo.User;
import com.qxxl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;

@CrossOrigin
@Controller
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    @ResponseBody
    //@RequestMapping(value = "/login",method = {RequestMethod.GET})
    public String login(User user){
        User tem_user = userService.queryByUsername(user.getUsername());
        if(tem_user != null && tem_user.getPassword().equals(user.getPassword())){
            return "success";
        }
        else{
            return "error";
        }
    }
}
