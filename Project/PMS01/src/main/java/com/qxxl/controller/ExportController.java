package com.qxxl.controller;

import com.qxxl.pojo.Employee;
import com.qxxl.pojo.FormerEmployee;
import com.qxxl.service.EmployeeService;
import com.qxxl.service.FormerEmployeeService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ExportController {
    @Autowired
    FormerEmployeeService formerEmployeeService;

    /**
     * 离职员工 报表
     * @param num
     * @param response
     * @return
     */
    @CrossOrigin
    @RequestMapping("/showLeave")
    public List<FormerEmployee> showByDept(Integer num, HttpServletResponse response) {
        List<FormerEmployee> formerEmployeeList = new ArrayList<FormerEmployee>(formerEmployeeService.fired(num));
        return formerEmployeeList;
    }

    @RequestMapping("/exportLeave")
    public void exportByDept(Integer num, HttpServletResponse response) {
        List<FormerEmployee> formerEmployeeList = new ArrayList<FormerEmployee>(formerEmployeeService.fired(num));
        String fileName = "部门离职员工报表.xls";
        OutputStream outputStream = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            //设置ContentType请求信息格式
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("Dept");//创建一张表
            Row titleRow = sheet.createRow(0);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("员工id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("员工性别");
            titleRow.createCell(3).setCellValue("员工生日");
            titleRow.createCell(4).setCellValue("原工作部门");
            titleRow.createCell(5).setCellValue("原工作岗位");
            titleRow.createCell(6).setCellValue("离职日期");
            int cell = 1;
            for (FormerEmployee formerEmployee : formerEmployeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(formerEmployee.getFormerEmployeeId());
                row.createCell(1).setCellValue(formerEmployee.getFormerEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(formerEmployee.getSex() ? "男" : "女");
                row.createCell(3).setCellValue((formerEmployee.getBirthday().getYear() + 1900) + "-" + (formerEmployee.getBirthday().getMonth() + 1) + "-" + (formerEmployee.getBirthday().getDate()));
                row.createCell(4).setCellValue(formerEmployee.getOriginDeptId());
                row.createCell(5).setCellValue(formerEmployee.getOriginJobId());
                row.createCell(6).setCellValue((formerEmployee.getLeavedate().getYear() + 1900) + "-" + (formerEmployee.getLeavedate().getMonth() + 1) + "-" + (formerEmployee.getLeavedate().getDate()));
                cell++;
            }
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }







    @Autowired
    EmployeeService employeeService;

    /**
     * 新聘员工 报表
     * @param num
     * @param response
     * @return
     */
    @CrossOrigin
    @RequestMapping("/showEmploy")
    public List<Employee> showByEmploy(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.Employ(num));
        System.out.println("===========");
        System.out.println(employeeList);
        System.out.println("============");
        return employeeList;
    }

    @RequestMapping("/exportEmploy")
    public void exportByEmploy(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.Employ(num));
        String fileName = "部门新聘员工报表.xls";
        OutputStream outputStream = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            //设置ContentType请求信息格式
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("Dept");//创建一张表
            Row titleRow = sheet.createRow(0);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("id卡编号");
            titleRow.createCell(3).setCellValue("员工性别");
            titleRow.createCell(4).setCellValue("电话号码");
            titleRow.createCell(5).setCellValue("生日");
            titleRow.createCell(6).setCellValue("email");
            titleRow.createCell(7).setCellValue("聘用日期");
            titleRow.createCell(8).setCellValue("部门岗位转变日期");
            titleRow.createCell(9).setCellValue("部门id");
            titleRow.createCell(10).setCellValue("岗位id");
            titleRow.createCell(11).setCellValue("原部门id");
            titleRow.createCell(12).setCellValue("原岗位id");
            titleRow.createCell(13).setCellValue("月工资id");
            titleRow.createCell(14).setCellValue("生涯信息id");
            titleRow.createCell(15).setCellValue("教育信息id");
            titleRow.createCell(16).setCellValue("婚姻状况");
            int cell = 1;
            for (Employee employee : employeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(employee.getEmployeeId());
                row.createCell(1).setCellValue(employee.getEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(employee.getIdCard());
                row.createCell(3).setCellValue(employee.getSex() ? "男" : "女");
                row.createCell(4).setCellValue(employee.getPhoneNum());
                row.createCell(5).setCellValue((employee.getBirthday().getYear() + 1900) + (employee.getBirthday().getMonth() + 1) + (employee.getBirthday().getDate()));
                row.createCell(6).setCellValue(employee.getEmail());
                row.createCell(7).setCellValue((employee.getHiredate().getYear() + 1900) + (employee.getHiredate().getMonth() + 1) + (employee.getHiredate().getDate()));
                row.createCell(7).setCellValue((employee.getTransferDate().getYear() + 1900) + (employee.getTransferDate().getMonth() + 1) + (employee.getTransferDate().getDate()));
                row.createCell(9).setCellValue(employee.getDeptId());
                row.createCell(10).setCellValue(employee.getJobId());
                row.createCell(11).setCellValue(employee.getOriginDeptId());
                row.createCell(12).setCellValue(employee.getOriginJobId());
                row.createCell(13).setCellValue(employee.getMonthlySalaryId());
                row.createCell(14).setCellValue(employee.getCareerId());
                row.createCell(15).setCellValue(employee.getEducationInfoId());
                row.createCell(16).setCellValue(employee.getMarried());
                cell++;
            }
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 岗位调动 报表
     * @param num
     * @param response
     * @return
     */
    @CrossOrigin
    @RequestMapping("/showDeptTran")
    public List<Employee> showdeptTran(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.deptTran(num));
        return employeeList;
    }


    @RequestMapping("/exportDeptTran")
    public void exportdeptTran(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.deptTran(num));
        String fileName = "部门调动员工报表.xls";
        OutputStream outputStream = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            //设置ContentType请求信息格式
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("Dept");//创建一张表
            Row titleRow = sheet.createRow(0);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("id卡编号");
            titleRow.createCell(3).setCellValue("员工性别");
            titleRow.createCell(4).setCellValue("电话号码");
            titleRow.createCell(5).setCellValue("生日");
            titleRow.createCell(6).setCellValue("email");
            titleRow.createCell(7).setCellValue("聘用日期");
            titleRow.createCell(8).setCellValue("部门岗位转变日期");
            titleRow.createCell(9).setCellValue("部门id");
            titleRow.createCell(10).setCellValue("岗位id");
            titleRow.createCell(11).setCellValue("原部门id");
            titleRow.createCell(12).setCellValue("原岗位id");
            titleRow.createCell(13).setCellValue("月工资id");
            titleRow.createCell(14).setCellValue("生涯信息id");
            titleRow.createCell(15).setCellValue("教育信息id");
            titleRow.createCell(16).setCellValue("婚姻状况");
            int cell = 1;
            for (Employee employee : employeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(employee.getEmployeeId());
                row.createCell(1).setCellValue(employee.getEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(employee.getIdCard());
                row.createCell(3).setCellValue(employee.getSex() ? "男" : "女");
                row.createCell(4).setCellValue(employee.getPhoneNum());
                row.createCell(5).setCellValue((employee.getBirthday().getYear() + 1900) + (employee.getBirthday().getMonth() + 1) + (employee.getBirthday().getDate()));
                row.createCell(6).setCellValue(employee.getEmail());
                row.createCell(7).setCellValue((employee.getHiredate().getYear() + 1900) + (employee.getHiredate().getMonth() + 1) + (employee.getHiredate().getDate()));
                row.createCell(7).setCellValue((employee.getTransferDate().getYear() + 1900) + (employee.getTransferDate().getMonth() + 1) + (employee.getTransferDate().getDate()));
                row.createCell(9).setCellValue(employee.getDeptId());
                row.createCell(10).setCellValue(employee.getJobId());
                row.createCell(11).setCellValue(employee.getOriginDeptId());
                row.createCell(12).setCellValue(employee.getOriginJobId());
                row.createCell(13).setCellValue(employee.getMonthlySalaryId());
                row.createCell(14).setCellValue(employee.getCareerId());
                row.createCell(15).setCellValue(employee.getEducationInfoId());
                row.createCell(16).setCellValue(employee.getMarried());
                cell++;
            }
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 部门调动 报表
     * @param num
     * @param response
     * @return
     */
    @CrossOrigin
    @RequestMapping("/showJobTran")
    public List<Employee> showJobTran(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.jobTran(num));
        System.out.println("===========");
        System.out.println(employeeList);
        System.out.println("================");
        return employeeList;
    }

    @RequestMapping("/exportJobTran")
    public void exportJobTran(Integer num, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.jobTran(num));
        String fileName = "岗位调动员工报表.xls";
        OutputStream outputStream = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            //设置ContentType请求信息格式
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("Dept");//创建一张表
            Row titleRow = sheet.createRow(0);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("id卡编号");
            titleRow.createCell(3).setCellValue("员工性别");
            titleRow.createCell(4).setCellValue("电话号码");
            titleRow.createCell(5).setCellValue("生日");
            titleRow.createCell(6).setCellValue("email");
            titleRow.createCell(7).setCellValue("聘用日期");
            titleRow.createCell(8).setCellValue("部门岗位转变日期");
            titleRow.createCell(9).setCellValue("部门id");
            titleRow.createCell(10).setCellValue("岗位id");
            titleRow.createCell(11).setCellValue("原部门id");
            titleRow.createCell(12).setCellValue("原岗位id");
            titleRow.createCell(13).setCellValue("月工资id");
            titleRow.createCell(14).setCellValue("生涯信息id");
            titleRow.createCell(15).setCellValue("教育信息id");
            titleRow.createCell(16).setCellValue("婚姻状况");
            int cell = 1;
            for (Employee employee : employeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(employee.getEmployeeId());
                row.createCell(1).setCellValue(employee.getEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(employee.getIdCard());
                row.createCell(3).setCellValue(employee.getSex() ? "男" : "女");
                row.createCell(4).setCellValue(employee.getPhoneNum());
                row.createCell(5).setCellValue((employee.getBirthday().getYear() + 1900) + (employee.getBirthday().getMonth() + 1) + (employee.getBirthday().getDate()));
                row.createCell(6).setCellValue(employee.getEmail());
                row.createCell(7).setCellValue((employee.getHiredate().getYear() + 1900) + (employee.getHiredate().getMonth() + 1) + (employee.getHiredate().getDate()));
                row.createCell(7).setCellValue((employee.getTransferDate().getYear() + 1900) + (employee.getTransferDate().getMonth() + 1) + (employee.getTransferDate().getDate()));
                row.createCell(9).setCellValue(employee.getDeptId());
                row.createCell(10).setCellValue(employee.getJobId());
                row.createCell(11).setCellValue(employee.getOriginDeptId());
                row.createCell(12).setCellValue(employee.getOriginJobId());
                row.createCell(13).setCellValue(employee.getMonthlySalaryId());
                row.createCell(14).setCellValue(employee.getCareerId());
                row.createCell(15).setCellValue(employee.getEducationInfoId());
                row.createCell(16).setCellValue(employee.getMarried());
                cell++;
            }
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 人事月报
     * @param month
     * @param response
     */
    @RequestMapping("/exportMonthly")
    public void exportMonthly(Integer month, HttpServletResponse response) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeService.monthEmployee(month));
        List<FormerEmployee> formerEmployeeList = new ArrayList<FormerEmployee>(formerEmployeeService.monthFired(month));
        String fileName = "人事月报.xls";
        OutputStream outputStream = null;
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8");
            //设置ContentType请求信息格式
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("Dept");//创建一张表
            Row titleRow = sheet.createRow(0);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("id卡编号");
            titleRow.createCell(3).setCellValue("员工性别");
            titleRow.createCell(4).setCellValue("电话号码");
            titleRow.createCell(5).setCellValue("生日");
            titleRow.createCell(6).setCellValue("email");
            titleRow.createCell(7).setCellValue("聘用日期");
            titleRow.createCell(8).setCellValue("部门岗位转变日期");
            titleRow.createCell(9).setCellValue("部门id");
            titleRow.createCell(10).setCellValue("岗位id");
            titleRow.createCell(11).setCellValue("原部门id");
            titleRow.createCell(12).setCellValue("原岗位id");
            titleRow.createCell(13).setCellValue("月工资id");
            titleRow.createCell(14).setCellValue("生涯信息id");
            titleRow.createCell(15).setCellValue("教育信息id");
            titleRow.createCell(16).setCellValue("婚姻状况");
            int cell = 1;
            for (Employee employee : employeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(employee.getEmployeeId());
                row.createCell(1).setCellValue(employee.getEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(employee.getIdCard());
                row.createCell(3).setCellValue(employee.getSex() ? "男" : "女");
                row.createCell(4).setCellValue(employee.getPhoneNum());
                row.createCell(5).setCellValue((employee.getBirthday().getYear() + 1900) + "-" + (employee.getBirthday().getMonth() + 1) + "-" + (employee.getBirthday().getDate()));
                row.createCell(6).setCellValue(employee.getEmail());
                row.createCell(7).setCellValue((employee.getHiredate().getYear() + 1900) + "-" + (employee.getHiredate().getMonth() + 1) + "-" + (employee.getHiredate().getDate()));
                row.createCell(8).setCellValue((employee.getTransferDate().getYear() + 1900) + "-" + (employee.getTransferDate().getMonth() + 1) + "-" + (employee.getTransferDate().getDate()));
                row.createCell(9).setCellValue(employee.getDeptId());
                row.createCell(10).setCellValue(employee.getJobId());
                if (employee.getOriginDeptId() != null) {
                    row.createCell(11).setCellValue(employee.getOriginDeptId());
                }
                if (employee.getOriginJobId() != null) {
                    row.createCell(12).setCellValue(employee.getOriginJobId());
                }
                row.createCell(13).setCellValue(employee.getMonthlySalaryId());
                row.createCell(14).setCellValue(employee.getCareerId());
                row.createCell(15).setCellValue(employee.getEducationInfoId());
                row.createCell(16).setCellValue(employee.getMarried());
                cell++;
            }
            titleRow = sheet.createRow(cell);//创建第一行，起始为0
            titleRow.createCell(0).setCellValue("员工id");//第一列
            titleRow.createCell(1).setCellValue("员工名字");
            titleRow.createCell(2).setCellValue("员工性别");
            titleRow.createCell(3).setCellValue("员工生日");
            titleRow.createCell(4).setCellValue("原工作部门");
            titleRow.createCell(5).setCellValue("原工作岗位");
            titleRow.createCell(6).setCellValue("离职日期");
            cell++;
            for (FormerEmployee formerEmployee : formerEmployeeList) {
                Row row = sheet.createRow(cell);//从第二行开始保存数据
                row.createCell(0).setCellValue(formerEmployee.getFormerEmployeeId());
                row.createCell(1).setCellValue(formerEmployee.getFormerEmployeeName());//将数据库的数据遍历出来
                row.createCell(2).setCellValue(formerEmployee.getSex() ? "男" : "女");
                row.createCell(3).setCellValue((formerEmployee.getBirthday().getYear() + 1900) + "-" + (formerEmployee.getBirthday().getMonth() + 1) + "-" + (formerEmployee.getBirthday().getDate()));
                row.createCell(4).setCellValue(formerEmployee.getOriginDeptId());
                row.createCell(5).setCellValue(formerEmployee.getOriginJobId());
                row.createCell(6).setCellValue((formerEmployee.getLeavedate().getYear() + 1900) + "-" + (formerEmployee.getLeavedate().getMonth() + 1) + "-" + (formerEmployee.getLeavedate().getDate()));
                cell++;
            }
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            outputStream = response.getOutputStream();
            wb.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
