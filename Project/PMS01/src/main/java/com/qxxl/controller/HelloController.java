package com.qxxl.controller;

import com.qxxl.pojo.User;
import com.qxxl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
public class HelloController {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserService userService;

    @GetMapping("/user/{username}")
    public User queryByUsername(@PathVariable String username){
        return userService.queryByUsername(username);
    }

    @GetMapping("hello")
    public String hello(){
        System.out.println(dataSource);
        return "Hello, Spring Boot!";
    }
}
