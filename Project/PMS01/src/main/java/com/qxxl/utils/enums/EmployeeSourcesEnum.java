package com.qxxl.utils.enums;

import java.util.HashMap;

public enum EmployeeSourcesEnum {
    // 人才库，普通招聘，校园招聘
    TALENT_POOL("人才库", 0), NORMAL("普通招聘", 1), CAMPUS("校园招聘", 2);

    EmployeeSourcesEnum(String key, Integer vaule) {
        this.key = key;
        this.value = vaule;
    }

    private String key;
    private Integer value;

    private static HashMap<Integer, EmployeeSourcesEnum> vauleMap = new HashMap<Integer, EmployeeSourcesEnum>();

    static{
        for (EmployeeSourcesEnum item : EmployeeSourcesEnum.values()){
            vauleMap.put(item.getValue(), item);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static EmployeeSourcesEnum getByValue(int value){
        EmployeeSourcesEnum result = vauleMap.get(value);
        if(result == null){
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }
}
