package com.qxxl.utils.enums;

import lombok.Getter;

import java.util.HashMap;

@Getter
public enum EmployeeTypesEnum {
    // 正式员工，临时员工，实习员工，试用员工
    // 只有试用员工能够转正
    FORMAL("正式员工", 0), TEMPORARY("临时员工", 1), TRAINING("实习员工", 2), PROBATIONARY("试用员工", 3);

    EmployeeTypesEnum(String key, Integer vaule) {
        this.key = key;
        this.value = vaule;
    }

    private String key;
    private Integer value;

    private static HashMap<Integer, EmployeeTypesEnum> vauleMap = new HashMap<Integer, EmployeeTypesEnum>();

    static{
        for (EmployeeTypesEnum item : EmployeeTypesEnum.values()){
            vauleMap.put(item.getValue(), item);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static EmployeeTypesEnum getByValue(int value){
        EmployeeTypesEnum result = vauleMap.get(value);
        if(result == null){
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }
}
