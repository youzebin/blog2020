package com.qxxl.utils.enums;

import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.stereotype.Component;


public enum EmployeeForeignLanguageAbilityEnum {
    // 初中，高中，四级，六级，专四，专八，托福，雅思。暂时先这么多
    JUNIOR("初中", 0), SENIOR("高中", 1), CET4("大学四级", 2), CET6("大学六级", 3),
    TEM4("专业四级", 4), TEM8("专业八级", 5), TOEFL("托福", 6), IELTS("雅思", 7);

    EmployeeForeignLanguageAbilityEnum(String key, Integer vaule) {
        this.key = key;
        this.value = vaule;
    }

    private String key;
    private Integer value;

    private static HashMap<Integer, EmployeeForeignLanguageAbilityEnum> vauleMap = new HashMap<Integer, EmployeeForeignLanguageAbilityEnum>();

    static{
        for (EmployeeForeignLanguageAbilityEnum item : EmployeeForeignLanguageAbilityEnum.values()){
            vauleMap.put(item.getValue(), item);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static EmployeeForeignLanguageAbilityEnum getByValue(int value){
        EmployeeForeignLanguageAbilityEnum result = vauleMap.get(value);
        if(result == null){
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }
}
