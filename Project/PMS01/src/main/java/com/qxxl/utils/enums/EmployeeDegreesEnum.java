package com.qxxl.utils.enums;

import java.util.HashMap;

public enum EmployeeDegreesEnum {
    // 高中学历，大专，本科，硕士，博士
    HIGH("高中", 0), COLLEGE("大专", 1), BACHELOR("学士", 2), MASTER("硕士", 3), DOCTOR("博士", 4);

    EmployeeDegreesEnum(String key, Integer vaule) {
        this.key = key;
        this.value = vaule;
    }

    private String key;
    private Integer value;

    private static HashMap<Integer, EmployeeDegreesEnum> vauleMap = new HashMap<Integer, EmployeeDegreesEnum>();

    static{
        for (EmployeeDegreesEnum item : EmployeeDegreesEnum.values()){
            vauleMap.put(item.getValue(), item);
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public static EmployeeDegreesEnum getByValue(int value){
        EmployeeDegreesEnum result = vauleMap.get(value);
        if(result == null){
            throw new IllegalArgumentException("No element matches " + value);
        }
        return result;
    }
}
