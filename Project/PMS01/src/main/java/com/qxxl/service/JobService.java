package com.qxxl.service;

import com.qxxl.mapper.JobMapper;
import com.qxxl.pojo.FormerEmployee;
import com.qxxl.pojo.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class JobService {

    @Autowired
    private JobMapper jobMapper;

    public Job queryById(Integer id){
        return jobMapper.selectByPrimaryKey(id);
    }

    public List<Job> queryByName(String name){
        return jobMapper.selectByName(name);
    }

    @Transactional
    public void saveJob(Job job){
        jobMapper.insertSelective(job);
    }

    @Transactional
    public void deleteJob(Integer id){
        jobMapper.deleteByPrimaryKey(id);
    }

    @Transactional
    public void updateJob(Job job){
        jobMapper.updateByPrimaryKeySelective(job);
    }

    @Transactional
    public List<Job> jobGetAll()
    {
        return jobMapper.selectAll();
    }

    public String jobCheck(Job job){
        if (job.getJobName() == null) {
            return "岗位名称为空";
        }

        // 岗位人数默认值0
        return "success";
    }

    public String jobIdCheck(Integer id){

        if (jobMapper.selectByPrimaryKey(id) == null) {
            return "不存在此岗位";
        }

        return "success";
    }
}
