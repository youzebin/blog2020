package com.qxxl.service;

import com.qxxl.mapper.DeptMapper;
import com.qxxl.pojo.Career;
import com.qxxl.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class DeptService {

    @Autowired
    private DeptMapper deptMapper;

    public Dept queryById(Integer id){
        return deptMapper.selectByPrimaryKey(id);
    }

    @Transactional
    public void saveDept(Dept dept){
        // 选择性新增，如果属性为空，不会出现在Insert语句上
        deptMapper.insertSelective(dept);
    }

    @Transactional
    public void deleteDept(Integer id){
        deptMapper.deleteByPrimaryKey(id);
    }

    @Transactional
    public void updateDept(Dept dept){
        // 只会更新不为null的字段
        deptMapper.updateByPrimaryKeySelective(dept);
    }

    @Transactional
    public List<Dept> deptGetAll()
    {
        return deptMapper.selectAll();
    }

    @Transactional
    public List<Dept> queryByName(String name)
    {
        return deptMapper.selectByName(name);
    }

    public String deptCheck(Dept dept){

        if (dept.getDeptName() == null) {
            return "部门名称为空";
        }

        // 人数有默认值0，无需检测
        return "success";
    }

    public String deptIdCheck(Integer id){

        if (deptMapper.selectByPrimaryKey(id) == null) {
            return "不存在此部门";
        }

        return "success";
    }
}
