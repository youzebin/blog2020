package com.qxxl.service;

//import com.qxxl.exception.OperationFailedException;

import com.qxxl.mapper.*;
import com.qxxl.pojo.*;
import com.qxxl.utils.enums.EmployeeSourcesEnum;
import com.qxxl.utils.enums.EmployeeTypesEnum;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private DeptMapper deptMapper;

    @Autowired
    private FormerEmployeeMapper formerEmployeeMapper;

    @Autowired
    private EducationInfoMapper educationInfoMapper;

    @Autowired
    private CareerMapper careerMapper;

    @Autowired
    private SalaryMapper salaryMapper;

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    public Employee queryById(Integer id) {

        return employeeMapper.selectByPrimaryKey(id);
    }

    /**
     * 通过名称模糊查询
     *
     * @param name
     * @return
     */
    public List<Employee> queryByName(String name) {

        return employeeMapper.selectByName(name);
    }

    /**
     * 通过部门id查询
     *
     * @param deptId
     * @return
     */
    public List<Employee> queryByDeptId(Integer deptId) {

        return employeeMapper.selectByDeptId(deptId);
    }

    /**
     * 通过岗位id查询
     *
     * @param jobId
     * @return
     */
    public List<Employee> queryByJobId(Integer jobId) {

        return employeeMapper.selectByJobId(jobId);
    }

    /**
     * 保存一个employee
     *
     * @param employee
     */
    @Transactional
    public void saveEmployee(Employee employee) {

        employeeMapper.insertSelective(employee);
    }

    /**
     * 删除一个employee
     *
     * @param id
     */
    @Transactional
    public void deleteEmployee(Integer id) {

        employeeMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新一个employee
     *
     * @param employee
     */
    @Transactional
    public void updateEmployee(Employee employee) {

        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 返回所有employee的List
     *
     * @return
     */
    public List<Employee> findAll() {

        return employeeMapper.selectAll();
    }

    public List<Employee> findJobTran(){
        return employeeMapper.findJobTran();
    }

    public List<Employee> findDeptTran(){
        return employeeMapper.findDeptTran();
    }

    public Career getCareer(Integer employeeId){

        return careerMapper.selectByPrimaryKey(employeeMapper.selectByPrimaryKey(employeeId).getCareerId());
    }

    public EducationInfo getEducationInfo(Integer employeeId){

        return educationInfoMapper.selectByPrimaryKey(employeeMapper.selectByPrimaryKey(employeeId).getEducationInfoId());
    }

    public Salary getSalary(Integer employeeId){

        return salaryMapper.selectByPrimaryKey(employeeMapper.selectByPrimaryKey(employeeId).getMonthlySalaryId());
    }

    /**
     * 普通入职，参数是一个employee
     *
     * @param employee
     */
    @Transactional
    public void normalEmploy(Employee employee) {

        // 1 设置来源，普通来源
        //employee.setSource(EmployeeSourcesEnum.NORMAL);
        // 2 设置入职时间，今天
        employee.setHiredate(new Date(System.currentTimeMillis()));
        // 3 设置originJobId和originDeptId为null
        employee.setOriginJobId(null);
        employee.setOriginDeptId(null);
        // 4 job和dept人数-1
        addJobEmployeeNum(employee.getJobId(), 1);
        addDeptEmployeeNum(employee.getDeptId(), 1);
        // 新建对应的salary career educationInfo，
        // 设置salaryId careerId educationInfoId
        createCareerEducationInfoSalary(employee);
        // 5 tb_employee加入
        employeeMapper.insertSelective(employee);
    }

    /**
     * 试用期员工转正
     *
     * @param employeeId
     */
    @Transactional
    public void empConfirm(int employeeId) {

        Employee employee = employeeMapper.selectByPrimaryKey(employeeId);
        if (employee.getEmployeeTypes() == null) {
            throw new IllegalArgumentException("NULL TYPES");
        } else if (employee.getEmployeeTypes().equals(EmployeeTypesEnum.PROBATIONARY)) {
            // 是试用期员工
            employee.setEmployeeTypes(EmployeeTypesEnum.FORMAL);
            employeeMapper.updateByPrimaryKeySelective(employee);
        } else {
            // 不是试用期员工
            throw new IllegalArgumentException(employee.getEmployeeTypes() + " NOT PROBATIONARY");
        }
    }

    /**
     * 岗位调动
     *
     * @param employeeId
     * @param jobId
     */
    @Transactional
    public void jobTransfer(int employeeId, int jobId) {

        // 1 取出该员工
        Employee employee = employeeMapper.selectByPrimaryKey(employeeId);
        // 2 修改员工originJobId和jobId
        employee.setOriginJobId(employee.getJobId());
        employee.setJobId(jobId);
        // 3 修改源job的人数和目标job的人数
        addJobEmployeeNum(employee.getOriginJobId(), -1);
        addJobEmployeeNum(employee.getJobId(), 1);
        // 4 更新employee
        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 部门调动
     *
     * @param employeeId
     * @param deptId
     */
    @Transactional
    public void deptTransfer(int employeeId, int deptId) {

        // 1 取出该员工
        Employee employee = employeeMapper.selectByPrimaryKey(employeeId);
        // 2 修改员工originDeptId和deptId
        employee.setOriginDeptId(employee.getDeptId());
        employee.setDeptId(deptId);
        // 3 修改源dept人数和目标dept人数
        addDeptEmployeeNum(employee.getOriginDeptId(), -1);
        addDeptEmployeeNum(employee.getDeptId(), 1);
        // 4 更新employee
        employeeMapper.updateByPrimaryKeySelective(employee);
    }

    /**
     * 通过某些值来找到Employee
     * 比如 查询 名字含李的已调动员工  会返回 列表 列表中包含名字有李的已调动员工。
     *
     * @param employeeName
     * @return
     */
    @Transactional
    public List<Employee> jobTranSearch(String employeeName) {
        List<Employee> employees = employeeMapper.selectByName(employeeName);
        for (Employee em : employees) {
            if (em.getOriginDeptId() != null || em.getOriginJobId() != null) {
                continue;
            } else {
                employees.remove(em);
            }
        }
        return employees;
    }

    /**
     * 员工离职
     *
     * @param employeeId
     */
    @Transactional
    public void empLeave(int employeeId) {
        // 1 取出该员工
        Employee employee = employeeMapper.selectByPrimaryKey(employeeId);
        // 2 初始化一个FormerEmployee对象
        FormerEmployee formerEmployee = new FormerEmployee();
        formerEmployee.setOriginJobId(employee.getJobId());
        formerEmployee.setSex(employee.getSex());
        formerEmployee.setOriginDeptId(employee.getDeptId());
        formerEmployee.setBirthday(employee.getBirthday());
        formerEmployee.setLeavedate(new Date());
        formerEmployee.setFormerEmployeeName(employee.getEmployeeName());
        // 3 job和dept人数-1
        addDeptEmployeeNum(employee.getDeptId(), -1);
        addJobEmployeeNum(employee.getJobId(), -1);
        // 4 移除该员工
        employeeMapper.deleteByPrimaryKey(employeeId);
        // 5 在tb_former_employee添加该员工
        formerEmployeeMapper.insertSelective(formerEmployee);
    }

    /**
     * 增加某个job的人数
     *
     * @param jobId
     * @param num
     */
    @Transactional
    public void addJobEmployeeNum(Integer jobId, Integer num) {

        // 1 获取岗位
        Job job = jobMapper.selectByPrimaryKey(jobId);

        // 2 修改jobNum
        int jobNum = job.getEmployeeNum() + num;
        // 判断jobNum是否合法，小于0？
        // ToDo

        job.setEmployeeNum(jobNum);

        // 3 存入新Job
        jobMapper.updateByPrimaryKeySelective(job);
    }

    /**
     * 增加某个dept的人数
     *
     * @param deptId
     * @param num
     */
    @Transactional
    public void addDeptEmployeeNum(Integer deptId, Integer num) {

        // 1 获取部门
        Dept dept = deptMapper.selectByPrimaryKey(deptId);

        // 2 修改deptNum
        int deptNum = dept.getEmployeeNum() + num;
        // 判断deptNum是否合法
        // TODO

        dept.setEmployeeNum(deptNum);

        // 3 存入新dept
        deptMapper.updateByPrimaryKeySelective(dept);
    }

    /**
     * 员工检查
     *
     * @param employee
     * @return
     */
    public String employeeCheck(Employee employee) {
        if (employee.getJobId() == null || jobMapper.selectByPrimaryKey(employee.getJobId()) == null) {
            return "岗位不存在";
        }
        if (employee.getDeptId() == null || deptMapper.selectByPrimaryKey(employee.getDeptId()) == null) {
            return "部门不存在";
        }
        if (employee.getEmployeeTypes() == null) {
            return "员工种类为空";
        }
        if (employee.getSex() == null) {
            return "员工性别为空";
        }
        if (employee.getMarried() == null) {
            return "员工婚姻情况为空";
        }
        if (employee.getBirthday() == null) {
            return "员工生日为空";
        }
        Date today = new Date(System.currentTimeMillis());
        if (employee.getBirthday().after(today)) {
            return "员工生日不合法";
        }
        if (employee.getSource() == null) {
            return "员工来源为空";
        }
        if (employee.getDegree() == null) {
            return "员工学历为空";
        }
        if (employee.getEmail() == null) {
            return "员工Email为空";
        }
        if (employee.getIdCard() == null) {
            return "员工身份证为空";
        }
        if (employee.getPhoneNum() == null) {
            return "员工电话为空";
        }

        return "success";
    }

    /**
     * 查询id是否存在
     *
     * @param employeeId
     * @return
     */
    public String employeeIdCheck(Integer employeeId) {

        if (employeeMapper.selectByPrimaryKey(employeeId) == null) {
            return "员工不存在";
        }

        return "success";
    }

    /**
     * 获取指定日期范围内的所有员工
     *
     * @param num
     * @return
     */
    @Transactional
    public List<Employee> Employ(Integer num) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeMapper.findAll());
        //System.out.println(formerEmployeeMapper.findAll());
        int month = 1;
        switch (num) {
            case 1:
                month = 1;
                break;
            case 2:
                month = 3;
                break;
            case 3:
                month = 6;
                break;
            case 4:
                month = 12;
                break;
        }
        List<Employee> employees = new ArrayList<Employee>();
        java.util.Date today = new java.util.Date(120, 6, 3);
        for (Employee employee : employeeList) {
            //System.out.println(formerEmployee);
            //System.out.println(formerEmployee.getLeavedate().getDate());
            int todaymonth = today.getMonth() + 12 * (today.getYear() - employee.getHiredate().getYear());
            if (todaymonth - employee.getHiredate().getMonth() < month) {
                employees.add(employee);
            } else if (todaymonth - employee.getHiredate().getMonth() == month) {
                if (today.getDate() <= employee.getHiredate().getDate()) {
                    employees.add(employee);
                }
            }
        }
        return employees;
    }

    /**@Transactional
    public List<Employee> findDeptTran(){

        return employeeMapper.findDeptTran();
    }

    @Transactional
    public List<Employee> findJobTran(){

        return employeeMapper.findJobTran();
    }**/

    /**
     * 获取指定日期范围内调动过部门的员工
     *
     * @param num
     * @return
     */
    @Transactional
    public List<Employee> deptTran(Integer num) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeMapper.findDeptTran());
        int month = 1;
        switch (num) {
            case 1:
                month = 1;
                break;
            case 2:
                month = 3;
                break;
            case 3:
                month = 6;
                break;
            case 4:
                month = 12;
                break;
        }
        List<Employee> employees = new ArrayList<Employee>();
        java.util.Date today = new java.util.Date(120, 6, 3);
        for (Employee employee : employeeList) {
            //System.out.println(formerEmployee);
            //System.out.println(formerEmployee.getLeavedate().getDate());
            int todaymonth = today.getMonth() + 12 * (today.getYear() - employee.getHiredate().getYear());
            if (todaymonth - employee.getHiredate().getMonth() < month) {
                employees.add(employee);
            } else if (todaymonth - employee.getHiredate().getMonth() == month) {
                if (today.getDate() <= employee.getHiredate().getDate()) {
                    employees.add(employee);
                }
            }
        }
        return employees;
    }

    /**
     * 获取指定日期内调动过job的员工
     *
     * @param num
     * @return
     */
    @Transactional
    public List<Employee> jobTran(Integer num) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeMapper.findJobTran());
        //System.out.println(formerEmployeeMapper.findAll());
        int month = 1;
        switch (num) {
            case 1:
                month = 1;
                break;
            case 2:
                month = 3;
                break;
            case 3:
                month = 6;
                break;
            case 4:
                month = 12;
                break;
        }
        List<Employee> employees = new ArrayList<Employee>();
        java.util.Date today = new java.util.Date(120, 6, 3);
        for (Employee employee : employeeList) {
            //System.out.println(formerEmployee);
            //System.out.println(formerEmployee.getLeavedate().getDate());
            int todaymonth = today.getMonth() + 12 * (today.getYear() - employee.getHiredate().getYear());
            if (todaymonth - employee.getHiredate().getMonth() < month) {
                employees.add(employee);
            } else if (todaymonth - employee.getHiredate().getMonth() == month) {
                if (today.getDate() <= employee.getHiredate().getDate()) {
                    employees.add(employee);
                }
            }
        }
        return employees;
    }

    /**
     * 获取近一个月的员工
     * @param month
     * @return
     */
    @Transactional
    public List<Employee> monthEmployee(Integer month) {
        List<Employee> employeeList = new ArrayList<Employee>(employeeMapper.findAll());
        List<Employee> employees = new ArrayList<Employee>();
        for (Employee employee : employeeList) {
            if (employee.getHiredate().getMonth() == month - 1 || employee.getTransferDate().getMonth() == month - 1) {
                employees.add(employee);
            }
        }
        return employees;
    }

    /**
     * 返回试用期员工列表
     * @return
     */
    @Transactional
    public List<Employee> getProbationaryEmployees(){

        List<Employee> employees = employeeMapper.findAll();
        List<Employee> probationaryEmployees = new ArrayList<Employee>();

        for (Employee employee : employees) {
            if(employee.getEmployeeTypes().equals(EmployeeTypesEnum.PROBATIONARY)){

                probationaryEmployees.add(employee);
            }
        }

        return probationaryEmployees;
    }

    /**
     * 返回正式员工列表
     * @return
     */
    @Transactional
    public List<Employee> getFormalEmployees(){

        List<Employee> employees = employeeMapper.findAll();
        List<Employee> formalEmployees = new ArrayList<Employee>();

        for (Employee employee : employees) {
            if(employee.getEmployeeTypes().equals(EmployeeTypesEnum.FORMAL)){

                formalEmployees.add(employee);
            }
        }
        return formalEmployees;
    }



    @Transactional
    public void createCareerEducationInfoSalary(Employee employee){

        Career career = new Career();
        careerMapper.insertSelective(career);
        employee.setCareerId(careerMapper.returnLastId());

        Salary salary = new Salary();
        salaryMapper.insertSelective(salary);
        employee.setMonthlySalaryId(salaryMapper.returnLastId());

        EducationInfo educationInfo = new EducationInfo();
        educationInfoMapper.insertSelective(educationInfo);
        employee.setEducationInfoId(educationInfoMapper.returnLastId());

    }
}
