package com.qxxl.service;

import com.qxxl.mapper.*;
import com.qxxl.pojo.*;
import com.qxxl.utils.enums.EmployeeSourcesEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TalentService {

    @Autowired
    private TalentMapper talentMapper;

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private DeptMapper deptMapper;

    @Autowired
    private CareerMapper careerMapper;

    @Autowired
    private EducationInfoMapper educationInfoMapper;

    @Autowired
    private SalaryMapper salaryMapper;

    public Talent queryById(Integer id){
        return talentMapper.selectByPrimaryKey(id);
    }

    public List<Talent> queryByName(String name){
        return talentMapper.selectByName(name);
    }

    @Transactional
    public void saveTalent(Talent talent){
        talentMapper.insertSelective(talent);
    }

    @Transactional
    public void deleteTalent(Integer id){
        talentMapper.deleteByPrimaryKey(id);
    }

    @Transactional
    public void updateTalent(Talent talent){
        talentMapper.updateByPrimaryKeySelective(talent);
    }

    @Transactional
    public void talentEmploy(Integer talent_id){

        Employee employee = new Employee();
        employee.setJobId(1);
        employee.setDeptId(1);

        // 1 人才库找到此人
        Talent talent = talentMapper.selectByPrimaryKey(talent_id);

        // 2 把此人基本信息添加到employee中
        talentEmployGetBasicInfo(talent, employee);
        // 至此，talent已经没用，应当在employee中存入其他信息

        // 3 设置员工来源，入职日期
        employee.setSource(EmployeeSourcesEnum.TALENT_POOL);
        employee.setHiredate(new Date(System.currentTimeMillis()));

        // 4 人才库移除此人
        talentMapper.deleteByPrimaryKey(talent_id);

        // 5 Job，Dept人数+1
        addJobEmployeeNum(employee.getJobId(), 1);
        addDeptEmployeeNum(employee.getDeptId(), 1);

        // 6 OriginJobId, OriginDeptId检查，置0
        employee.setOriginDeptId(null);
        employee.setOriginJobId(null);

        // 7 创建career educationinfo salary
        createCareerEducationInfoSalary(employee);

        // 8 员工表加入此人
        employeeMapper.insertSelective(employee);

    }

    @Transactional
    public void talentEmployGetBasicInfo(Talent talent, Employee employee){
        employee.setEmployeeName(talent.getTalentName());
        employee.setIdCard(talent.getIdCard());
        employee.setSex(talent.getSex());
        employee.setPhoneNum(talent.getPhoneNum());
        employee.setBirthday(talent.getBirthday());
        employee.setEmail(talent.getEmail());
    }

    @Transactional
    public void addJobEmployeeNum(Integer jobId, Integer num){

        // 1 获取岗位
        Job job = jobMapper.selectByPrimaryKey(jobId);

        // 2 修改jobNum
        int jobNum = job.getEmployeeNum() + num;
        // 判断jobNum是否合法，小于0？
        // ToDo

        job.setEmployeeNum(jobNum);

        // 3 存入新Job
        jobMapper.updateByPrimaryKeySelective(job);
    }

    @Transactional
    public void addDeptEmployeeNum(Integer deptId, Integer num){

        // 1 获取部门
        Dept dept = deptMapper.selectByPrimaryKey(deptId);

        // 2 修改deptNum
        int deptNum = dept.getEmployeeNum() + num;
        // 判断deptNum是否合法
        // TODO

        dept.setEmployeeNum(deptNum);

        // 3 存入新dept
        deptMapper.updateByPrimaryKeySelective(dept);
    }

    public String talentCheck(Talent talent){

        if (talent.getTalentName() == null) {
            return "人才姓名为空";
        }
        if (talent.getBirthday() == null) {
            return "人才生日为空";
        }
        Date today = new Date(System.currentTimeMillis());
        if(talent.getBirthday().after(today)){
            return "人才生日不合法";
        }
        if (talent.getEmail() == null) {
            return "人才Email为空";
        }
        if (talent.getIdCard() == null) {
            return "人才身份证为空";
        }
        if (talent.getPhoneNum() == null) {
            return "人才电话为空";
        }
        if (talent.getSex() == null) {
            return "人才性别为空";
        }

        return "success";
    }

    public String talentIdCheck(Integer talentId){

        if (talentMapper.selectByPrimaryKey(talentId) == null) {
            return "人才不存在";
        }

        return "success";
    }

    /**
     * 返回所有Talent的List
     *
     * @return
     */
    public List<Talent> findAll() {

        return talentMapper.selectAll();
    }

    @Transactional
    public void createCareerEducationInfoSalary(Employee employee){

        Career career = new Career();
        careerMapper.insertSelective(career);
        employee.setCareerId(careerMapper.returnLastId());

        Salary salary = new Salary();
        salaryMapper.insertSelective(salary);
        employee.setMonthlySalaryId(salaryMapper.returnLastId());

        EducationInfo educationInfo = new EducationInfo();
        educationInfoMapper.insertSelective(educationInfo);
        employee.setEducationInfoId(educationInfoMapper.returnLastId());

    }
}
