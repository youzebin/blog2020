package com.qxxl.service;

import com.qxxl.mapper.EducationInfoMapper;
import com.qxxl.pojo.Career;
import com.qxxl.pojo.EducationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EducationInfoService {

    @Autowired
    private EducationInfoMapper educationInfoMapper;

    public EducationInfo queryById(Integer id){
        return educationInfoMapper.selectByPrimaryKey(id);
    }

    @Transactional
    public void saveEducationInfo(EducationInfo educationInfo){
        educationInfoMapper.insertSelective(educationInfo);
    }

    @Transactional
    public Integer saveEducationInfoAndReturnId(EducationInfo educationInfo){

        educationInfoMapper.insertSelective(educationInfo);
        return educationInfoMapper.returnLastId();
    }

    @Transactional
    public void deleteEducationInfo(Integer id){
        educationInfoMapper.deleteByPrimaryKey(id);
    }

    @Transactional
    public void updateEducationInfo(EducationInfo educationInfo){
        educationInfoMapper.updateByPrimaryKeySelective(educationInfo);
    }

    public String educationInfoCheck(EducationInfo educationInfo){
        return "success";
    }
}
