package com.qxxl.service;

import com.qxxl.mapper.UserMapper;
import com.qxxl.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public User queryByUsername(String username){
        return userMapper.selectByPrimaryKey(username);
    }

    @Transactional
    public void deleteUser(String username){
        userMapper.deleteByPrimaryKey(username);
    }

    @Transactional
    public void updateUser(User user){
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Transactional
    public void saveUser(User user){
        // 选择性新增，如果属性为空，不会出现在Insert语句上
        userMapper.insertSelective(user);
    }

    public String userCheck(User user){
        if (user.getUsername() == null) {
            return "用户名为空";
        }
        if (user.getPassword() == null) {
            return "用户密码为空";
        }

        return "success";
    }
}
