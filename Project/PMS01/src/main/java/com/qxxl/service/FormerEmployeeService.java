package com.qxxl.service;

import com.qxxl.mapper.FormerEmployeeMapper;
import com.qxxl.pojo.FormerEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FormerEmployeeService {

    @Autowired
    private FormerEmployeeMapper formerEmployeeMapper;

    public FormerEmployee queryById(Integer id) {
        return formerEmployeeMapper.selectByPrimaryKey(id);
    }

    public List<FormerEmployee> queryByName(String name) {
        return formerEmployeeMapper.selectByName(name);
    }

    /**
     * 保存一个离职员工
     * @param formerEmployee
     */
    @Transactional
    public void saveFormerEmployee(FormerEmployee formerEmployee) {
        formerEmployeeMapper.insertSelective(formerEmployee);
    }

    /**
     * 删除一个离职员工
     * @param id
     */
    @Transactional
    public void deleteFormerEmployee(Integer id) {
        formerEmployeeMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新一个离职员工
     * @param formerEmployee
     */
    @Transactional
    public void updateFormerEmployee(FormerEmployee formerEmployee) {
        formerEmployeeMapper.updateByPrimaryKeySelective(formerEmployee);
    }

    /**
     * 离职员工格式检查
     * @param formerEmployee
     * @return
     */
    public String formerEmployeeCheck(FormerEmployee formerEmployee) {

        // 只在离职的时候产生，复制Employee的信息，应该不会出错
        return "success";
    }

    /**
     * 离职员工id存在性检查
     * @param formerEmployeeId
     * @return
     */
    public String formerEmployeeIdCheck(Integer formerEmployeeId) {

        if (formerEmployeeMapper.selectByPrimaryKey(formerEmployeeId) == null) {
            return "离职员工不存在";
        }

        return "success";
    }

    /**
     * 获取指定日期内离职员工列表
     * @param num
     * @return
     */
    @Transactional
    public List<FormerEmployee> fired(Integer num) {
        List<FormerEmployee> formerEmployeeList = new ArrayList<FormerEmployee>(formerEmployeeMapper.findAll());
        //System.out.println(formerEmployeeMapper.findAll());
        int month = 1;
        switch (num) {
            case 1:
                month = 1;
                break;
            case 2:
                month = 3;
                break;
            case 3:
                month = 6;
                break;
            case 4:
                month = 12;
                break;
        }
        List<FormerEmployee> formerEmployees = new ArrayList<FormerEmployee>();
        Date today = new Date(120, 6, 3);
        for (FormerEmployee formerEmployee : formerEmployeeList) {
            System.out.println(formerEmployee);
            System.out.println(formerEmployee.getLeavedate().getDate());
            int todaymonth = today.getMonth() + 12 * (today.getYear() - formerEmployee.getLeavedate().getYear());
            if (todaymonth - formerEmployee.getLeavedate().getMonth() < month) {
                formerEmployees.add(formerEmployee);
            } else if (todaymonth - formerEmployee.getLeavedate().getMonth() == month) {
                if (today.getDate() <= formerEmployee.getLeavedate().getDate()) {
                    formerEmployees.add(formerEmployee);
                }
            }
        }
        return formerEmployees;
    }

    /**
     * 获取近一个月离职员工列表
     * @param month
     * @return
     */
    @Transactional
    public List<FormerEmployee> monthFired(Integer month)
    {
        List<FormerEmployee> formerEmployeeList=new ArrayList<FormerEmployee>(formerEmployeeMapper.findAll());
        List<FormerEmployee> formerEmployees=new ArrayList<FormerEmployee>();
        for(FormerEmployee formerEmployee:formerEmployeeList)
        {
            if(formerEmployee.getLeavedate().getMonth()==month-1)
            {
                formerEmployees.add(formerEmployee);
            }
        }
        return formerEmployees;
    }
}
