package com.qxxl.service;

import com.qxxl.mapper.CareerMapper;
import com.qxxl.pojo.Career;
import com.qxxl.pojo.Salary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CareerService {

    @Autowired
    private CareerMapper careerMapper;

    public Career queryById(Integer id){
        return careerMapper.selectByPrimaryKey(id);
    }

    @Transactional
    public void saveCareer(Career career){
        // 选择性新增，如果属性为空，不会出现在Insert语句上
        careerMapper.insertSelective(career);
    }

    @Transactional
    public Integer saveCareerAndReturnId(Career career){

        careerMapper.insertSelective(career);
        return careerMapper.returnLastId();
    }

    @Transactional
    public void deleteCareer(Integer id){
        careerMapper.deleteByPrimaryKey(id);
    }

    @Transactional
    public void updateCareer(Career career){
        // 只会更新不为null的字段
        careerMapper.updateByPrimaryKeySelective(career);
    }

    public String careerCheck(Career career){

        // 不强制填
        return "success";
    }
}
