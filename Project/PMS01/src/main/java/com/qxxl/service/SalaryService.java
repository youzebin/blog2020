package com.qxxl.service;

import com.qxxl.mapper.SalaryMapper;
import com.qxxl.pojo.Salary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;

@Service
public class SalaryService {

    @Autowired
    private SalaryMapper salaryMapper;

    public Salary queryById(Integer id) {

        return salaryMapper.selectByPrimaryKey(id);
    }

    @Transactional
    public void saveSalary(Salary salary) {

        salaryMapper.insertSelective(salary);
    }

    @Transactional
    public Integer saveSalaryAndReturnId(Salary salary){

        salaryMapper.insertSelective(salary);
        return salaryMapper.returnLastId();
    }

    @Transactional
    public void updateSalary(Salary salary) {

        salaryMapper.updateByPrimaryKeySelective(salary);
    }

    @Transactional
    public void deleteSalary(Integer id) {

        salaryMapper.deleteByPrimaryKey(id);
    }

    public String salaryCheck(Salary salary) {

        if (salary.getBasicSalary() == null) {
            return "工资为空";
        }

        return "success";
    }
}
