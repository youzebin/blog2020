package com.qxxl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * spring boot工程引导类，工程的入口类，一般取名Application
 * 工程启动注解，这是一个组合注解
 */
@SpringBootApplication
// 扫描Mybatis所有的mapper接口，指定包路径。
@MapperScan("com.qxxl.mapper")
@ComponentScan(basePackages = {"com.qxxl.controller","com.qxxl.service"})
public class Application {

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
